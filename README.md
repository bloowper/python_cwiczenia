# Zadania na cwiczenia z pythona

# zestaw 7 

### 7.1
ZADANIE 7.1 (KLASA FRAC)
W pliku fracs.py zdefiniować klasę Frac wraz z potrzebnymi metodami. Wykorzystać wyjątek ValueError do obsługi błędów w ułamkach. Dodać możliwości dodawania liczb (int, long) do ułamków (działania lewostronne i prawostronne). Rozważyć możliwość włączenia liczb float do działań na ułamkach [Wskazówka: metoda float.as_integer_ratio()]. Napisać kod testujący moduł fracs.

### 7.2
W pliku polys.py zdefiniować klasę Poly wraz z potrzebnymi metodami. Wykorzystać wyjątek ValueError do obsługi błędów w wielomianach. Dodać możliwości dodawania liczb (int, long, float) do wielomianów (działania lewostronne i prawostronne). Dodanie nowych metod kontenerowych i sprawdzenie możliwości iteracji po instancji (for coeff in poly). Napisać kod testujący moduł polys.

### 7.3
W pliku rectangles.py zdefiniować klasę Rectangle wraz z potrzebnymi metodami. Wykorzystać wyjątek ValueError do obsługi błędów. Napisać kod testujący moduł rectangles.

### 7.4 
W pliku triangles.py zdefiniować klasę Triangle wraz z potrzebnymi metodami. Wykorzystać wyjątek ValueError do obsługi błędów. Napisać kod testujący moduł triangles.

### 7.5
W pliku circles.py zdefiniować klasę Circle wraz z potrzebnymi metodami. Okrąg jest określony przez podanie środka i promienia. Wykorzystać wyjątek ValueError do obsługi błędów. Napisać kod testujący moduł circles.

# zestaw 8

OBOWIĄZKOWE DO PRZESŁANIA: 8.1 (implementacja), 8.3, 8.4, 8.6
### ZADANIE 8.1
Zbadać problem szukania rozwiązań równania liniowego postaci a * x + b * y + c = 0. Podać specyfikację problemu. Podać algorytm rozwiązania w postaci listy kroków, schematu blokowego, drzewa. Podać implementację algorytmu w Pythonie w postaci funkcji solve1(), która rozwiązania wypisuje w formie komunikatów.
### ZADANIE 8.2
Zbadać problem szukania rozwiązań równania kwadratowego postaci a * x * x + b * x + c = 0. Podać specyfikację problemu. Podać algorytm rozwiązania w postaci listy kroków, schematu blokowego, drzewa. Podać implementację algorytmu w Pythonie w postaci funkcji solve2(), która rozwiązania wypisuje w formie komunikatów.

def solve2(a, b, c):
    """Rozwiązywanie równania kwadratowego a * x * x + b * x + c = 0."""
    pass

### ZADANIE 8.3
Obliczyć liczbę pi za pomocą algorytmu Monte Carlo. Wykorzystać losowanie punktów z kwadratu z wpisanym kołem. Sprawdzić zależność dokładności wyniku od liczby losowań. Wskazówka: Skorzystać z modułu random.

def calc_pi(n=100):
    """Obliczanie liczby pi metodą Monte Carlo.
    n oznacza liczbę losowanych punktów."""
    pass

### ZADANIE 8.4
Zaimplementować algorytm obliczający pole powierzchni trójkąta, jeżeli dane są trzy liczby będące długościami jego boków. Jeżeli podane liczby nie spełniają warunku trójkąta, to program ma generować wyjątek ValueError.

def heron(a, b, c):
    """Obliczanie pola powierzchni trójkąta za pomocą wzoru
    Herona. Długości boków trójkąta wynoszą a, b, c."""
    pass

### ZADANIE 8.5
Zbadać zachowanie funkcji Ackermanna dla p = 1, 2, 3, 4 i różnych n. Wartości funkcji zebrać w tabeli.

def ackermann(n, p):
    """Funkcja Ackermanna, n i p to liczby naturalne.
    Zachodzi A(0, p) = 1, A(n, 1) = 2n, A(n, 2) = 2 ** n,
    A(n, 3) = 2 ** A(n-1, 3).
    """
    if n == 0:
        return 1
    if p == 0 and n >= 0:
        if n == 1:
            return 2
        else:
            return n + 2
    if p >= 0 and n >= 1:
        return ackermann(ackermann(n-1, p), p-1)

### ZADANIE 8.6
Za pomocą techniki programowania dynamicznego napisać program obliczający wartości funkcji P(i, j). Porównać z wersją rekurencyjną programu. Wskazówka: Wykorzystać tablicę dwuwymiarową (np. słownik) do przechowywania wartości funkcji. Wartości w tablicy wypełniać kolejno wierszami.

# zestaw 9

ADANIE 9.1 (SINGLELIST)
Do klasy SingleList dodać nowe metody.

class SingleList:
# ... inne metody ...

    def remove_tail(self): pass   # klasy O(N)
        # Zwraca cały węzeł, skraca listę.
        # Dla pustej listy rzuca wyjątek ValueError.

    def merge(self, other): pass   # klasy O(1)
        # Węzły z listy other są przepinane do listy self na jej koniec.
        # Po zakończeniu operacji lista other ma być pusta.

    def clear(self): pass     # czyszczenie listy
ZADANIE 9.2 (SINGLELIST)
Do klasy SingleList dodać nowe metody.

class SingleList:
# ... inne metody ...

    def search(self, data): pass   # klasy O(N)
        # Zwraca łącze do węzła o podanym kluczu lub None.

    def find_min(self): pass   # klasy O(N)
        # Zwraca łącze do węzła z najmniejszym kluczem lub None dla pustej listy.

    def find_max(self): pass   # klasy O(N)
        # Zwraca łącze do węzła z największym kluczem lub None dla pustej listy.

    def reverse(self): pass   # klasy O(N)
        # Odwracanie kolejności węzłów na liście.
ZADANIE 9.3 (DOUBLELIST)
Do klasy DoubleList dopisać nowe metody.

class DoubleList:

    def find_max(self): pass
        # Zwraca łącze do węzła z największym kluczem lub None dla pustej listy.

    def find_min(self): pass
        # Zwraca łącze do węzła z najmniejszym kluczem lub None dla pustej listy.

    def remove(self, node): pass
        # Usuwa wskazany węzeł z listy.

    def clear(self): pass     # czyszczenie listy
ZADANIE 9.4 (SORTEDLIST)
Na bazie list powiązanych pojedynczo lub podwójnie stworzyć klasę SortedList, która przechowuje listę stale posortowaną. Największy element jest na początku listy (head).

class SortedList:

    def __init__(self):
        self.head = None
        self.length = 0

    def is_empty(self):
        return self.head is None

    def insert(self, node): pass
        # L = SortedList() ; L.insert(Node(3))

    def remove(self): pass
        # Zwraca node z elementem największym, czyli head.
        # Długość listy zmniejsza się o jeden węzeł.

    def merge(self, other): pass
        # Scalanie dwóch list posortowanych.
        # Po tej operacji lista other ma być pusta.

    def clear(self): pass     # czyszczenie listy
ZADANIE 9.5 (CYCLICLIST)
Na bazie list powiązanych pojedynczo lub podwójnie stworzyć klasę CyclicList, która przechowuje listę cykliczną. Atrybut head może wskazywać na dowolny węzeł listy cyklicznej, natomiast przez tail rozumiemy węzeł bezpośrednio poprzedzający head.

class CyclicList:

    def __init__(self):
        self.head = None     # łącze do dowolnego węzła listy

    def is_empty(self):
        return self.head is None

    def insert_head(self, node): pass

    def insert_tail(self, node): pass

    def search(self, data):   # zwraca node lub None

    def remove(self, node): pass

    def merge(self, other): pass   # scalanie dwóch list cyklicznych w czasie O(1)

    def clear(self): pass     # czyszczenie listy
ZADANIE 9.6 (BINARYTREE)
Mamy drzewo binarne bez klasy BinarySearchTree. Napisać funkcję count_leafs(top), która liczy liście drzewa binarnego. Liście to węzły, które nie mają potomków. Można podać rozwiązanie rekurencyjne lub rozwiązanie iteracyjne, które jawnie korzysta ze stosu.

Załóżmy, że drzewo binarne przechowuje liczby. Napisać funkcję calc_total(top), która podaje sumę liczb przechowywanych w drzewie.

def count_leafs(top): pass

def count_total(top): pass
ZADANIE 9.7 (BINARYSEARCHTREE)
Dla drzewa BST napisać funkcje znajdujące największy i najmniejszy element przechowywany w drzewie. Mamy łącze do korzenia, nie ma klasy BinarySearchTree. Drzewo BST nie jest modyfikowane, a zwracana jest znaleziona wartość (węzeł). W przypadku pustego drzewa należy wyzwolić wyjątek ValueError lub zwrócić None.

def bst_max(top): pass

def bst_min(top): pass



