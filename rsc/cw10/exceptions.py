class EmptyException(ValueError):
    pass


class FullException(ValueError):
    pass

class BadShapeException(ValueError):
    pass