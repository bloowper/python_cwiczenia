import rsc.cw10.exceptions as ex


class Matrix:

    def __init__(self, rows=1, cols=1):
        self.rows = rows
        self.cols = cols
        self.data = [0] * rows * cols

    def __getitem__(self, pair):   # odczyt m[i,j]
        i, j = pair
        return self.data[i * self.cols + j]

    def __setitem__(self, pair, value):   # m[i,j] = value
        i, j = pair
        self.data[i * self.cols + j] = value

    def __add__(self, other):   # dodawanie macierzy
        """
        :type other: Matrix
        """
        if(self.cols != other.cols or self.rows != other.rows):
            raise ex.BadShapeException("Matrixes have to be same shape")
        matrix = Matrix(self.rows, self.cols)
        for w in range(0,self.rows):
            for k in range(0,self.cols):
                matrix[w,k] = self[w,k] + other[w,k]
        return matrix

    def __sub__(self, other):    # odejmowanie macierzy
        """
        :type other: Matrix
        """
        if (self.cols != other.cols or self.rows != other.rows):
            raise ex.BadShapeException("Matrixes have to be same shape")
        matrix = Matrix(self.rows, self.cols)
        for w in range(0, self.rows):
            for k in range(0, self.cols):
                matrix[w, k] = self[w, k] - other[w, k]
        return matrix

    #TODO BLEDNE DZIALANIE METODY -> WYMAGANA POPRAWA
    def __mul__(self, other):    # mnożenie macierzy
        """
        :type other: Matrix
        """
        if(self.cols != other.rows):
            raise ex.BadShapeException("Not allowed multiply for that shapes")
        matrix = Matrix(self.rows, other.cols)

        for r in range(0,matrix.rows):
            for c in range(0,matrix.cols):
                sum = 0
                for i in range(0,self.cols):
                    sum+=self[r,i]*other[i,c]
                matrix[r,c] = sum

        return matrix

    def __str__(self):
        return str([self.data[x * self.cols: x * self.cols + self.cols] for x in range(0, self.rows)])



    def as_array(self):
        return [self.data[x * self.cols: x * self.cols + self.cols] for x in range(0, self.rows)]


# Zastosowanie.
if __name__ == '__main__':
    m1 = Matrix(2,3)
    m2 = Matrix(3,2)

    m1[0,0] = 1 ;   m1[0,1] = 2    ;m1[0,2] = 3
    m1[1, 0] = 4;   m1[1,1] = 5    ;m1[1,2] = 6


    m2[0,0] = 5     ;m2[0,1] = 3
    m2[1,0] = 9     ;m2[1,1] = 2
    m2[2,0] = 10    ;m2[2,1] = 1

    #
    # oczekiwana wartosc tego mnozenia to
    #
    #
    m3 = m1 * m2
    print(m3)

