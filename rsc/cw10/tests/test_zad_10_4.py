from unittest import TestCase
from rsc.cw10.zad_10_4 import Queue
from rsc.cw10.exceptions import EmptyException, FullException


class TestQueue(TestCase):

    def setUp(self) -> None:
        self.queue = Queue(5)#size 5/ gdyby zmienila sie implementacja, chce zeby testy dalej przechodzily

    def test_is_empty(self):
        queue = self.queue
        self.assertTrue(queue.is_empty())
        self.assertRaises(EmptyException,lambda :queue.get())
        queue.put("data1")
        queue.put("data2")
        self.assertFalse(queue.is_empty())
        self.assertEqual("data1",queue.get())
        self.assertFalse(queue.is_empty())
        self.assertEqual("data2",queue.get())
        self.assertTrue(queue.is_empty())
        self.assertRaises(EmptyException,lambda :queue.get())


    def test_is_full(self):
        queue = self.queue
        self.assertFalse(queue.is_full())
        queue.put("data")
        self.assertFalse(queue.is_full())
        queue.put("data")
        self.assertFalse(queue.is_full())
        queue.put("data")
        self.assertFalse(queue.is_full())
        queue.put("data")
        self.assertFalse(queue.is_full())
        queue.put("data")
        self.assertTrue(queue.is_full())
        self.assertRaises(FullException,lambda :queue.put("data"))
        queue.get()
        self.assertFalse(queue.is_full())
        queue.get()
        queue.get()
        queue.get()
        queue.get()
        self.assertRaises(EmptyException,lambda :queue.get())


    def test_put_get(self):
        queue = self.queue
        queue.put("1")
        queue.put("2")
        queue.put("3")
        queue.put("4")
        queue.put("5")
        self.assertRaises(FullException,lambda :queue.put("data"))
        self.assertEqual("1",queue.get())
        self.assertEqual("2",queue.get())
        self.assertEqual("3",queue.get())
        self.assertEqual("4",queue.get())
        self.assertEqual("5",queue.get())
        self.assertRaises(EmptyException,lambda :queue.get())



