from unittest import TestCase
from rsc.cw10.zad_10_8 import RandomQueue
from rsc.cw10.exceptions import EmptyException, FullException


class TestRandomQueue(TestCase):
    def test_basic(self):
        queue = RandomQueue()
        self.assertTrue(queue.is_empty())
        queue.insert("elem")
        queue.insert("elem")
        queue.insert("elem")
        queue.insert("elem")
        self.assertFalse(queue.is_empty())
        queue.remove()
        queue.remove()
        queue.remove()
        queue.remove()
        self.assertTrue(queue.is_empty())
        self.assertRaises(EmptyException, lambda :queue.remove())

    def test_clear(self):
        queue = RandomQueue()
        queue.insert("elem")
        queue.insert("elem")
        queue.insert("elem")
        queue.insert("elem")
        self.assertFalse(queue.is_empty())
        queue.clear()
