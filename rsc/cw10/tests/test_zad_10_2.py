from unittest import TestCase
from rsc.cw10.zad_10_2 import Stack
from rsc.cw10.exceptions import EmptyException, FullException


class TestStack(TestCase):
    def test_is_empty(self):
        stack = Stack()
        self.assertTrue(stack.is_empty())
        stack.push("hej")
        self.assertFalse(stack.is_empty())
        stack.pop()
        self.assertTrue(stack.is_empty())
        self.assertRaises(EmptyException,lambda :stack.pop())


    def test_is_full(self):
        stack = Stack(3)
        self.assertFalse(stack.is_full())
        stack.push("data");
        self.assertFalse(stack.is_full())
        stack.push("data");
        self.assertFalse(stack.is_full())
        stack.push("data");
        self.assertTrue(stack.is_full())
        self.assertRaises(FullException,lambda :stack.push("data"))

    def test_push(self):
        pass

    def test_pop(self):
        pass
