from rsc.cw10.exceptions import EmptyException, FullException


class Stack:

    def __init__(self, size=10):
        self.items = size * [None]      # utworzenie tablicy
        self.n = 0                      # liczba elementów na stosie
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def push(self, data):
        if self.n == self.size:
            raise FullException("Stack is full")
        self.items[self.n] = data
        self.n += 1

    def pop(self):
        if self.n == 0:
            raise EmptyException("Stack is empty")
        self.n -= 1
        data = self.items[self.n]
        self.items[self.n] = None    # usuwam referencję
        return data

    def __iter__(self):
        return self

    def __next__(self):
        try:
            return self.pop()
        except EmptyException:
            raise StopIteration()

if __name__ == '__main__':
    stack = Stack()
    stack.push("0")
    stack.push("1")
    stack.push("2")
    stack.push("3")
    stack.push("4")
    stack.push("5")
    stack.push("6")
    stack.push("7")
    stack.push("8")
    stack.push("9")
    for elem in stack:
        print(elem)
#
    while True:
        try:
            stack.push("test")
        except FullException:
            break

    for e in stack:
        print(e)