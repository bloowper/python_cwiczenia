import random
from rsc.cw10.exceptions import EmptyException, FullException
import random

class RandomQueue(list):

    def __init__(self) -> None:
        super().__init__()

    def insert(self,item):
        self.append(item)

    def remove(self):
        if(len(self) == 0):
            raise EmptyException("RandomQueue is empty")
        randint = random.randint(0, len(self) - 1)
        temp = self.__getitem__(-1)
        self.__setitem__(-1,self.__getitem__(randint))
        self.__setitem__(randint,temp)
        return self.pop()


    def is_empty(self):
        return 0 == len(self)

    def is_full(self):
        #przy przeciazaniu listy ktora jest dynamiczna
        #wydaje mi sie ze ograniczenie rozmiaru bedzie sztuczne
        #Chyba ze zabraknie pamieci na maszynie, ale nie umiem tego obsluzyc :(
        return False

    def clear(self):
        list.clear(self)


if __name__ == '__main__':
    queue  = RandomQueue()
    queue.append("elem")
    queue.insert("hej")
    queue.insert("hej2")
    queue.insert("hej3")
    queue.insert("hej4")
    print(queue.remove())
    print(queue.remove())
    print(queue.remove())
    print(queue.remove())
    print(queue)
    print(len(queue))
    print(queue.remove())
    print(queue)
    queue.clear()
    print(queue)


