# ZADANIE 4.4
# Napisać iteracyjną wersję funkcji fibonacci(n) obliczającej n-ty wyraz ciągu Fibonacciego.


#1  1
#2  1
#3  2
#4  3
#5  5
#6  8
#7  13

def fibonacci(n):
    n1=1
    n2=1
    for i in range(n-2):
        temp=n2
        n2=n2+n1
        n1=temp
    return n2

if __name__ == '__main__':

    for i in range(1,20):
        print("{}->{}".format(i,fibonacci(i)))