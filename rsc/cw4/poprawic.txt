Zestaw 4 zaliczony.
4.5 Osobna funkcja do zamiany miejscami elementów to jednak przesada. W Pythonie wywołanie funkcji jest stosunkowo kosztowne, dlatego np. numpy robi operacje wektorowe.
Brak wersji rekurencyjnej.
4.7 Pomysł z zagnieżdżoną funkcją flat ma swoje zalety. W moich testach to drugie w kolejności najszybsze rozwiązanie, ponieważ nie ma wielokrotnego kopiowania elementów. Ale przy ogromnych listach najlepsze jest rozwiązanie z generatorami.