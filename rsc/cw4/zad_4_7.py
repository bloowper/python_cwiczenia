# ZADANIE 4.7
# Mamy daną sekwencję, w której niektóre z elementów mogą okazać się podsekwencjami, a takie zagnieżdżenia mogą
# się nakładać do nieograniczonej głębokości. Napisać funkcję flatten(sequence), która zwróci spłaszczoną listę
# wszystkich elementów sekwencji. Wskazówka: rozważyć wersję rekurencyjną, a sprawdzanie czy element jest sekwencją,
# wykonać przez isinstance(item, (list, tuple)).


def flatten(sequence):
    topseq=[]
    def flat(seq):
        nonlocal topseq
        for elem in seq:
            if isinstance(elem,(list,tuple)):
                flat(elem)
            else:
                topseq.append(elem)
    flat(sequence)
    return topseq
#czy takie uzywanie zagniezdzonej funkcji z nie lokalna zmienna jest traktowane ok w programowaniu?
#chcialem ograniczyc zuzycie pamieci na tworzenie dodatkowych list na przechowywanie fragmentow z sekwencji posrednich



if __name__ == '__main__':
    seq = [1, (2, 3), [], [4, (5, 6, 7)], 8, [9]]
    print(flatten(seq))
