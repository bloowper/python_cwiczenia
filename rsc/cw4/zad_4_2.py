# ZADANIE 4.2
# Rozwiązania zadań 3.5 i 3.6 z poprzedniego zestawu zapisać w postaci funkcji, które zwracają pełny string przez return.

def miarka(dlugosc):
    #z racji tego ze stringi sa immutable powinienem wykorzystac jakiegos string buildera
    #zeby zminimalizowac uzycie pamieci, ale pomine ta optymalizacje
    line1 = "|..."*dlugosc+"|"
    line2=""
    for i in range(dlugosc+1):
        if i == 0:
            line2+="0"
            continue
        line2+=f"{i:4}"
    return line1+"\n"+line2

#nie dziala do konca poprawnie. nie rysuje horyzontalnych lini.
#korzystam z tablicy gdyz stringi sa immutable, chciałem zminimalizowac tracenie pamieci
def prostokat(x,y):
    def array2dToStr(array):
        temp = ""
        for line in array:
            for char in line:
                temp += str(char)
            temp += "\n"
        return temp
    (xsize,ysize) = 5,5
    tablica = [[" "]*(x*xsize+1)]*(y*ysize+1)
    for wiersz in range(len(tablica)):
        for kolumna in range(len(tablica[wiersz])):
            if wiersz%ysize==0 and kolumna%xsize==0:
                tablica[wiersz][kolumna]="+"
            elif kolumna%xsize==0:
                tablica[wiersz][kolumna] = "|"
                #znudzilo mi sie szukanie dlaczego to sie nie wykonuje
            elif wiersz%ysize==0:
                tablica[wiersz][kolumna]="-"
    return array2dToStr(tablica)