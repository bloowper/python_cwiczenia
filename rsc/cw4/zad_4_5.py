# ZADANIE 4.5
# Napisać funkcję odwracanie(L, left, right) odwracającą kolejność
# elementów na liście od numeru left do right włącznie. Lista jest modyfikowan
# a w miejscu (in place). Rozważyć wersję iteracyjną i rekurencyjną.



#funkcja mutuje istniejaca liste => nie zwracamy nowej/ ewneutalnie mozna zwrocic ta sama by prosciej uzwac printa
#dla wygody bede zwracal referencje do tej samej listy, chociaz z racji mutowania istniejacej listy nie jest to wymagane
#bo zwracam referencje do obiektu ktory juz istenieje w zakresie w ktorym uzywam funkcji


#ZOSTALO DO NAPISANIA WERSJA REKURENCYJNA

def odwracanie(l,left,right):
    def swap(left,right):
        nonlocal l
        temp = l[left]
        l[left] = l[right]
        l[right]=temp
    while(left<right):
        swap(left,right)
        left+=1
        right-=1
    return l


if __name__ == '__main__':
    mylist = list(range(0,21))
    print(mylist)
    odwracanie(mylist,0,5)
    print(mylist)
    mylist2 = list(range(11))
    print(mylist2)
    print(odwracanie(mylist2,0,len(mylist2)-1))