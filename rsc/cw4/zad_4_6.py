# Napisać funkcję sum_seq(sequence) obliczającą sumę liczb
# zawartych w sekwencji, która może zawierać zagnieżdżone
# podsekwencje. Wskazówka: rozważyć wersję rekurencyjną, a
# sprawdzanie, czy element jest sekwencją, wykonać przez
# isinstance(item, (list, tuple)).

#LICZB, podkreslona. czyli moge sobie przyjac element poczatkowy bedacy zerem
#innaczej musial bym traktowac elementy bedace np stringami
#wartosc value nie mogla by byc 0
def sum_seq(sequence):
    value = 0
    for elem in sequence:
        if isinstance(elem,(list,tuple)):
            value+=sum_seq(elem)
        else:
            value+=elem
    return value



if __name__ == '__main__':
    myseq = [1,1,1,[1,1,1,[1,1,1,(1,1,1)]],1,1,1]#suma 15
    print(sum_seq(myseq))
    myseq2 = [1,-1,1,-1,1,-1,1,-1,[[1,-1,1,-1,[1,-1,1,-1,[1,-1,1,-1,[1,[-1,]]]]]]]
    print(sum_seq([myseq2]))