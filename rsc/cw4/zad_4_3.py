# ZADANIE 4.3
# Napisać iteracyjną wersję funkcji factorial(n) obliczającej silnię.

def factorial(n):
    start = 1
    for i in range(1,n+1):
        start*=i
    return start

if __name__ == '__main__':
    print(factorial(0))
    print(factorial(1))
    print(factorial(5))