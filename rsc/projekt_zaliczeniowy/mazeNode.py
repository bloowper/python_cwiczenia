from  node import Node,Direction,oppositeDirection

class MazeNode(Node):
    def __init__(self):
        super().__init__()
        self.unConnected = \
            {
            Direction.left,
            Direction.right,
            Direction.top,
            Direction.bottom
            }

    def __str__(self) -> str:
        """
        prawda albo falsz czy istnieje przejscie w dana strone
        """
        return f"[left {self.edges[Direction.left]!=None}][right {self.edges[Direction.right]!=None}][top {self.edges[Direction.top]!=None}][bottom {self.edges[Direction.bottom]!=None}]"

    def isConnected(self):
        if(
        self.edges[Direction.left]      != None or
        self.edges[Direction.right]     != None or
        self.edges[Direction.top]       != None or
        self.edges[Direction.bottom]    != None
        ) :
            return True
        return False

    def createConnection(self, direction, node):
        """
        :type direction: Direction
        :type node: MazeNode
        """
        super().createConnection(direction, node)
        self.unConnected.remove(direction)
        node.unConnected.remove(oppositeDirection(direction))

    #TODO funkcja do zastapienia przez zewnetrzna
    # rsc.projekt_zaliczeniowy.utilities.directionAbleToMove
    # rsc.projekt_zaliczeniowy.utilities.neighborhoodCoordinates
    def possibleDirections(self,wsp,mazeSize):
        """
        Return possible direction of move, only in maze range.
        returned set DONT INCLUDE dirrection currently CONNECTED to that node
        returned set INCLUDE neighbour currently in maze structure

        :param mazeSize: size of maze, node dont have any information about maze, we have to pass it
        :param wsp: information about co-ordinates
        :type wsp: tuple
        :type mazeSize: int
        """
        w,k = wsp
        list1 = []
        if(w-1 >= 0): list1.append(Direction.top)
        if(w+1 <mazeSize ): list1.append(Direction.bottom)
        if(k+1 < mazeSize): list1.append(Direction.right)
        if(k-1 >= 0): list1.append(Direction.left)
        return set(list1) & set(self.unConnected)

    def isConnectedInGivenDirection(self,dir):
        return self.edges[dir]!=None