import enum

class Direction(enum.Enum):
    top = 0
    bottom = 1
    left = 3
    right = 4

def oppositeDirection(direction):
    """
    :type direction: Direction
    """
    if direction == Direction.left:
        return Direction.right
    elif direction == Direction.right:
        return Direction.left
    elif direction == Direction.top:
        return Direction.bottom
    else:
        return Direction.top

def cordDeltaToDirection(start, end):
    """
    start -> cord2 . z start przechodzimy do cord2
    :rtype Direction
    :type start: tuple
    :type end: tuple
    """
    if end[1] > start[1] and end[0]==start[0]:
        return Direction.right
    if end[1] < start[1] and end[0]==start[0]:
        return Direction.left
    if end[0] > start[0] and end[1]==start[1]:
        return Direction.bottom
    if end[0] < start[0] and end[1]==start[1]:
        return Direction.top
    raise ValueError("cos poszlo nie tak. koordynaty musza lezec w jednym wierszu lub jednej kolumnie")

