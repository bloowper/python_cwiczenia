from direction import Direction
import copy
from allowed import *

# TODO function to remove
def cordDependOnDirrection(coords, dirrection):
    """

    :type coords: tuple
    :type dirrection: Direction
    """
    cords2 = list(copy.copy(coords))
    if dirrection == Direction.left:
        cords2[1] -= 1
        return cords2
    if dirrection == Direction.right:
        cords2[1] += 1
        return cords2
    if dirrection == Direction.top:
        cords2[0] -= 1
        return cords2
    if dirrection == Direction.bottom:
        cords2[0] += 1
        return cords2

def dirrectionDependOnCoordinatesDelta(coordStart, coordEnd):
    """

    :rtype: Direction
    :type coordStart: tuple
    :type coordEnd: tuple
    """
    r1,c1 = coordStart
    r2,c2 = coordEnd
    if r1>r2:
        return Direction.top
    if r1<r2:
        return Direction.bottom
    if c1>c2:
        return Direction.left
    if c1<c2:
        return Direction.right
    raise notAllowedDirrection()

def neighborhoodCoordinates(coords,mazesize):
    """
    :rtype: list[tuple]
    """
    row,col = coords
    toreturn = []
    if row-1>=0:
        toreturn.append((row-1,col))
    if row+1<mazesize:
        toreturn.append((row+1,col))
    if col-1>=0:
        toreturn.append((row,col-1))
    if col+1<mazesize:
        toreturn.append((row,col+1))
    return toreturn

def coordinatesAbleToMove(maze, coords) -> list:
    """
    :rtype: list
    :type maze: list[list[rsc.projekt_zaliczeniowy.mazeNode.MazeNode]]
    :type coords: tuple[int]
    """
    neighborhoodCords = neighborhoodCoordinates(coords,len(maze))
    avaiableNeighbors = [cord for cord in neighborhoodCords if not maze[cord[0]][cord[1]].isConnected()]
    return avaiableNeighbors