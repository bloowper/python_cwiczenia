# Generator labiryntu do postaci pliku png

## potrzebne zasoby

---
* numpy 
* cv2

# Rozruch programu

---
 poprzez plik maze.py z parametrami:
* python3 maze.py rozmiar(int) wielkosc_pomieszczenia(int)
* 1 rozmiar labiryntu
* 2 (opcjonalny) wielkosc jednego pomieszczenia w pixelach
 
    

## Działanie

---
przy pierwszym rozruchu po pobraniu programu z respozytorium zostanie utworzy folder /outputpng
do którego następnie będą  generowanie pliki .png zawierające wygenerowane labirynt

## Zastosowany algorytm

---
generowanie jest oparte o backtracking na grafie nie skierowanym gdzie min deg v = 1 max deg v = 4, dodatkowo w trakcie poruszania sie po strukturze grafu program modifikuje  tablice numppy reprezentującą strukturę grafu
robi to poprzez ustawiani pixeli na bialo ( co reprezentuje laczenie pobliskich chamberow/wierzcholkow/edges)
