import numpy as np
from mazeNode import MazeNode,Direction
from collections import deque
from utilities import *
from exceptions.allowed import *
import numpy
import random
from direction import *
import copy
import sys
import os
import time
import cv2
from datetime import datetime



DEBUG = False
clear = lambda: os.system('clear') # to nie dziala TERM environment variable not set.
clear2 = lambda :print("\n"*200)



class Maze:
    """
    Labirynt : po utworzeniu instancji obiektu nie istnieja w nim
    jeszcze zadne drogi.
    potrzebna jest inicjalizacja dróg w labiryncie

    """

    #show as String properties
    wall = "█"
    open = " "


    #Immage properties
    wallsize = 4 #pixel

    def __init__(self,size,wallsize):
        """
        :param size: size of 2D mymaze
        :type size: int
        after initialize: mymaze is not ready yet, there is no connections
        """

        #Maze structure
        Maze.wallsize = wallsize
        self.size = size
        self.startPoint = (0,0)
        self.numOfNodesInUse = 1
        self.maze = [[MazeNode() for x in range(size)] for y in range(size)]

        #Immage representation properties
        self.immage = np.zeros((self.size * Maze.wallsize * 3,self.size * Maze.wallsize *3,3))
        self.szerokoscZestawu = 3 * Maze.wallsize
        for r in range(self.size):
            for c in range(self.size):
                point = (r*self.szerokoscZestawu,c*self.szerokoscZestawu)
                #Srodek zestawu (pokoj w labiryncie)
                self.immage[    point[0] +  Maze.wallsize : point[0] +  2*Maze.wallsize,
                                point[1] +  Maze.wallsize : point[1] +  2*Maze.wallsize ] = 1

    def createAllStepsAtOnce(self):
        """
        creating edges between vertexes
        creating structure of mymaze on given 2D matrix (self.mymaze)
        """
        # zdecydowalem sie napisac dwie funkcje, jedna robiaca caly labirynt
        # na raz a druga tworzaca jedna poloczenie
        # dla potrzeb stworzenia pozniej jakiegos GUI
        # reprezentujacego proces tworzenia
        stack = deque()
        head = self.startPoint
        #poprawic logike zeby stack nie rzucal bledu ze jest pusty
        while self.numOfNodesInUse < self.size * self.size :
            cordsAbleToMove:list = coordinatesAbleToMove(self.maze, head)
            if len(cordsAbleToMove) == 0:
                head = stack.pop()
                continue
            choice = random.choice(cordsAbleToMove)
            #choice jest nowym elementem(tuple - koordynaty) ktory zostanie dolaczony do struktury
            self.connectNew(head,choice)
            stack.append(copy.copy(head))#Czy tutaj musze uzywac coppy czy by sie obylo bez?
            head = choice
            self.numOfNodesInUse+=1
        self.updateImmage()


    def connectNew(self,start,end):
        direction = cordDeltaToDirection(start, end)
        startR , startC = start
        endR, endC = end
        self.maze[startR][startC].createConnection(direction,self.maze[endR][endC])


    #TODO uzyc jakiegos string buildera <= ogromne marnowanie pamieci
    def mazeAsString(self):
        """
        for debug purpose
        return MULTILINE string representation of maze
        """
        # na kazdy wiersz komorek labiryntu bedzie sie skladac 3 wiersze reprezentacji ASCII
        def wallOrOpen(r, c, direction):
            return Maze.wall if (self.maze[r][c].edges[direction] == None) else Maze.open

        # TODO uzyc jakiegos string buildiera zeby nie marnowac tyle pamieci
        toReturn = ""
        for row in range(self.size):
            topline = ""
            midline = ""
            bottomline = ""
            for col in range(self.size):
                topline += Maze.wall
                topline += wallOrOpen(row, col, Direction.top)
                topline += Maze.wall

                midline += wallOrOpen(row, col, Direction.left)
                midline += Maze.open
                midline += wallOrOpen(row, col, Direction.right)

                bottomline += Maze.wall
                bottomline += wallOrOpen(row, col, Direction.bottom)
                bottomline += Maze.wall
            topline += "\n";
            midline += "\n";
            bottomline += "\n"
            toReturn += topline
            toReturn += midline
            toReturn += bottomline
        return toReturn

    def updateImmage(self):
        for r in range(self.size):
            for c in range(self.size):
                left = (r*Maze.wallsize*3 + Maze.wallsize,c*Maze.wallsize*3)
                right = (r*Maze.wallsize*3 + Maze.wallsize ,c*Maze.wallsize*3 + 2*Maze.wallsize)
                top = (r*Maze.wallsize*3,c*Maze.wallsize*3+Maze.wallsize)
                bottom = (r*Maze.wallsize*3 + 2*Maze.wallsize,c*Maze.wallsize*3+Maze.wallsize)
                if self.maze[r][c].isConnectedInGivenDirection(Direction.left):
                    self.immage[
                        left[0]:left[0]+maze.wallsize,
                        left[1]:left[1]+maze.wallsize
                    ] = 1
                if self.maze[r][c].isConnectedInGivenDirection(Direction.right):
                    self.immage[
                        right[0]:right[0] + maze.wallsize,
                        right[1]:right[1] + maze.wallsize
                    ] = 1
                if self.maze[r][c].isConnectedInGivenDirection(Direction.top):
                    self.immage[
                        top[0]:top[0] + maze.wallsize,
                        top[1]:top[1] + maze.wallsize
                    ] = 1
                if self.maze[r][c].isConnectedInGivenDirection(Direction.bottom):
                    self.immage[
                        bottom[0]:bottom[0] + maze.wallsize,
                        bottom[1]:bottom[1] + maze.wallsize
                    ] = 1


if __name__ == '__main__':
    consoleSize = None
    consoleWallSize = 4
    if len(sys.argv)==3:
        consoleSize = int(sys.argv[1])
        consoleWallSize = int(sys.argv[2])
    elif len(sys.argv)==2:
        consoleSize = int(sys.argv[1])
    else:
        raise ValueError("Argumenty musza zostac przekazane w postaci : arg0 - rozmiar arg1 - wielkosc pomieszczenia w pixelach (default = 4)")

    maze = Maze(consoleSize,consoleWallSize)
    maze.createAllStepsAtOnce()
    cv2.imshow("test",maze.immage)
    cv2.imwrite(f"outputpng/{datetime.today().strftime('%Y-%m-%d %H:%M:%S')}.png",maze.immage*255)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    # maze_as_string = maze.mazeAsString()
    # with open("maze.txt","w") as file:
    #     file.write(maze_as_string)
