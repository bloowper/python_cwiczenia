from unittest import TestCase
from rsc.projekt_zaliczeniowy.utilities import *
from rsc.projekt_zaliczeniowy.maze import *


# w niektorych testach wartosc oczekiwana jest w lini ponizej zeby oddzielic
# w bardziej widoczny sposob wywolania funkcji/metod od wartosci oczekiwanej
# pozwalalo mi to troche prosciej lapac sie w debugowaniu i pisaniu testow

class Test(TestCase):
    def test_neighborhood_coordinates(self):
        cords = (2, 2)
        mazesize = 5
        self.assertSetEqual(set(neighborhoodCoordinates(cords, mazesize)), {(1, 2), (3, 2), (2, 1), (2, 3)})
        cords = (0, 0)
        self.assertSetEqual(set(neighborhoodCoordinates(cords, mazesize)), {(1, 0), (0, 1)})
        cords = (4, 0)
        self.assertSetEqual(set(neighborhoodCoordinates(cords, mazesize)), {(3, 0), (4, 1)})
        cords = (4, 4)
        self.assertSetEqual(set(neighborhoodCoordinates(cords, mazesize)), {(3, 4), (4, 3)})
        cords = (0, 4)
        self.assertSetEqual(set(neighborhoodCoordinates(cords, mazesize)), {(0, 3), (1, 4)})


class Test2(TestCase):
    def test_coordinates_able_to_move(self):
        maze = Maze(5)
        maze.ConnectOnDirection((0, 2), Direction.bottom)
        maze.ConnectOnDirection((1, 2), Direction.left)
        maze.ConnectOnDirection((1, 1), Direction.bottom)

        # ███████████████
        # █ ██ ██ ██ ██ █
        # ███████ ███████
        # ███████ ███████
        # █ ██    ██ ██ █
        # ████ ██████████
        # ████ ██████████
        # █ ██ ██ ██ ██ █
        # ███████████████
        # ███████████████
        # █ ██ ██ ██ ██ █
        # ███████████████
        # ███████████████
        # █ ██ ██ ██ ██ █
        # ███████████████

        self.assertSetEqual(set(coordinatesAbleToMove(maze.maze, (0, 0))),
                            {(1, 0), (0, 1)})
        self.assertSetEqual(set(coordinatesAbleToMove(maze.maze, (1, 0))),
                            {(0, 0), (2, 0)})
        self.assertSetEqual(set(coordinatesAbleToMove(maze.maze, (3, 1))),
                            {(3, 0), (3, 2), (4, 1)})
        self.assertSetEqual(set(coordinatesAbleToMove(maze.maze, (3, 2))),
                            {(3, 1), (3, 3), (4, 2), (2, 2)})
        self.assertSetEqual(set(coordinatesAbleToMove(maze.maze, (2, 2))),
                            {(3, 2), (2, 3)})

        maze.ConnectOnDirection((1, 1), Direction.left)
        print(maze.mazeAsString())

class Test3(TestCase):
    def test_dirrection_depend_on_coordinates_delta(self):
        self.assertEqual(dirrectionDependOnCoordinatesDelta((5, 5), (5, 4)),
                         Direction.left)
        self.assertEqual(dirrectionDependOnCoordinatesDelta((5, 5), (5, 6)),
                         Direction.right)
        self.assertEqual(dirrectionDependOnCoordinatesDelta((5, 5), (4, 5)),
                         Direction.top)
        self.assertEqual(dirrectionDependOnCoordinatesDelta((5, 5), (6, 5)),
                         Direction.bottom)

