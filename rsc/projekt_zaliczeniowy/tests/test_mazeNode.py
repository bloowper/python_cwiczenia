from unittest import TestCase
from rsc.projekt_zaliczeniowy.direction import Direction
from rsc.projekt_zaliczeniowy.mazeNode import MazeNode
import unittest


class TestMazeNode(TestCase):
    def test_possible_directions(self):
        m1 = MazeNode()
        m2 = MazeNode()
        #sprawdze rogi dla NIE POLACZONEGO ELEMENTU
        self.assertCountEqual(m1.possibleDirections((0,0),5),{Direction.right,Direction.bottom})
        self.assertSetEqual(m1.possibleDirections((4,4),5),{Direction.left,Direction.top})
        self.assertSetEqual(m1.possibleDirections((4,0),5),{Direction.top,Direction.right})
        #kilka w srodku mazu| nie polaczony node
        self.assertSetEqual(m1.possibleDirections((2,2),5),{Direction.top,Direction.bottom,Direction.left,Direction.right})


        #M1<--->M2
        m1.createConnection(Direction.right,m2)
        self.assertEqual(m1.edges[Direction.right],m2)
        self.assertEqual(m2.edges[Direction.left],m1)
        self.assertSetEqual(m1.possibleDirections((3,3),10),{Direction.top,Direction.bottom,Direction.left})
        self.assertSetEqual(m2.possibleDirections((3,3),10),{Direction.top,Direction.right,Direction.bottom})


        #   CORNER LEFT TOP
        #------------------------
        #|M1<-->M2
        #|
        #|
        coordsMazeSizeM1 = ((0,0),10)
        coordsMazeSizeM2 = ((0,1),10)
        m1 = MazeNode()
        m2 = MazeNode()
        m1.createConnection(Direction.right,m2)
        self.assertSetEqual(m1.possibleDirections(*coordsMazeSizeM1),{Direction.bottom})
        self.assertSetEqual(m2.possibleDirections(*coordsMazeSizeM2),{Direction.bottom,Direction.right})


        #   CORNER RIGHT TOP
        #--------------
        #      M1<->M2|
        #             |
        #             |
        coordsMazeSizeM1 = ((0,8),10)
        coordsMazeSizeM2 = ((0,9),10)
        m1 = MazeNode()
        m2 = MazeNode()
        m1.createConnection(Direction.right,m2)
        self.assertSetEqual(m1.possibleDirections(*coordsMazeSizeM1),{Direction.left,Direction.bottom})
        self.assertSetEqual(m2.possibleDirections(*coordsMazeSizeM2),{Direction.bottom})


        #   CORNER LEFT BOTTOM
        # TODO napisac testy dla pozostalych rogow




