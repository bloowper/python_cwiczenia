from unittest import TestCase
from rsc.projekt_zaliczeniowy.direction import oppositeDirection,Direction
from rsc.projekt_zaliczeniowy.node import Node


class TestNode(TestCase):
    def test_create_connection(self):
        n1 = Node()
        n2 = Node()
        # N1 <-> N2
        n1.createConnection(Direction.right,n2)
        self.assertEqual(n1,n2.edges[Direction.left])
        self.assertEqual(n2,n1.edges[Direction.right])

        n1 = Node()
        n2 = Node()
        self.assertNotEqual(n1,n2.edges[Direction.left])
        self.assertNotEqual(n2,n1.edges[Direction.right])
        #   n1
        #   |
        #   n2
        n1.createConnection(Direction.bottom,n2)
        self.assertEqual(n1,n2.edges[Direction.top])
        self.assertEqual(n2,n1.edges[Direction.bottom])

        self.assertIsNone(n1.edges[Direction.left])
        self.assertIsNone(n1.edges[Direction.top])
        self.assertIsNone(n1.edges[Direction.right])

        self.assertIsNone(n2.edges[Direction.bottom])
        self.assertIsNone(n2.edges[Direction.left])
        self.assertIsNone(n2.edges[Direction.right])
