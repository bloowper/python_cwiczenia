from unittest import TestCase
from rsc.projekt_zaliczeniowy.direction import *


class Test(TestCase):
    def test_cord_delta_to_direction(self):
        # start -> end
        start = (5,5)
        end = (5,6)
        self.assertEqual(cordDeltaToDirection(start,end),Direction.right)
        start = (5,5)
        end = (5,4)
        self.assertEqual(cordDeltaToDirection(start,end),Direction.left)
        start = (5,5)
        end = (6,5)
        self.assertEqual(cordDeltaToDirection(start,end),Direction.bottom)
        start = (5,5)
        end = (4,5)
        self.assertEqual(cordDeltaToDirection(start,end),Direction.top)

