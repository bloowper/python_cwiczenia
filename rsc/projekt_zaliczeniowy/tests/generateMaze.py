import datetime
import sys
import rsc.projekt_zaliczeniowy.maze as maze
import cv2



class IllegalArgumentError(ValueError):
    pass


if __name__ == '__main__':
    mazeSize = None
    wallSize = 4
    if len(sys.argv)==3:
        mazeSize = int(sys.argv[1])
        wallSize = int(sys.argv[2])
    elif len(sys.argv)==2:
        mazeSize = int(sys.argv[1])
    else:
        raise IllegalArgumentError("Argumenty musza zostac przekazane w postaci : arg0 - rozmiar arg1 - wielkosc pomieszczenia w pixelach (default = 4)")

    myMaze = maze.Maze(mazeSize)
    myMaze.wallsize = wallSize
    myMaze.createAllStepsAtOnce()
    cv2.imshow("labirynt",myMaze.maze)
    cv2.imwrite(f"outputpng/{datetime.today().strftime('%Y-%m-%d %H:%M:%S')}.png", myMaze.immage * 255)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

