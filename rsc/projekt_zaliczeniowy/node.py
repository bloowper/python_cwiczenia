import enum
from direction import Direction
from direction import oppositeDirection

class Node:
    def __init__(self):
        self.edges = \
        {
        Direction.left : None,
        Direction.right : None,
        Direction.top : None,
        Direction.bottom : None
        }


    def createConnection(self,direction,node):
        """
        create node connection in indicated direction
        second node have set connection as well
        :type direction: Direction
        :type node: Node
        """
        self.edges[direction] = node
        node.edges[oppositeDirection(direction)] = self

