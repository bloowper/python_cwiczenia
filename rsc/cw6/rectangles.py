from rsc.cw6.points import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1, y1, x2, y2):
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return "[{}, {}]".format(self.pt1.__str__(), self.pt2.__str__())

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return f"{self.__class__.__name__}({self.__dict__})"
        # zmienilem bo chcialem zobaczyc czy tak mozna to rozwiazac
        # a nie mialem w sumie okazji wczesniej uzywac self.__dict__

    def __eq__(self, other):  # obsługa rect1 == rect2
        """
        :type other: Rectangle
        """
        if not isinstance(other, Rectangle):
            return False
        return self.pt1 == other.pt1 and self.pt2 == other.pt2

    def __ne__(self, other):  # obsługa rect1 != rect2
        return not self == other

    def center(self):  # zwraca środek prostokąta
        """
        :rtype: Point
        """
        return Point((self.pt1.x + self.pt2.x)/2,(self.pt1.y + self.pt2.y)/2)

    def area(self):  # pole powierzchni
        a = abs(self.pt1.x - self.pt2.x)
        b = abs(self.pt1.y - self.pt2.y)
        return a*b

    def move(self, x, y):  # przesunięcie o (x, y)
        m = Point(x, y)
        self.pt1 = self.pt1 + m
        self.pt2 = self.pt2 + m
        return self

if __name__ == '__main__':
    rectangle = Rectangle(1, 1, 2, 2)
    print(rectangle)
    print(rectangle.__repr__())
    repr