from unittest import TestCase
from rsc.cw6.times import Time

class TestTime(TestCase):

    def test_times(self):
        t1 = Time(3600)
        t2 = Time(3661)
        t3 = Time(3600)
        #tutaj oczekiwany powinien byc zamieniony z wywolaniem
        self.assertEqual(-1,t1.__cmp__(t2))
        self.assertEqual(1,t2.__cmp__(t1))
        self.assertEqual(0,t1.__cmp__(t1))
        self.assertEqual(0,t1.__cmp__(t3))

    def test_dunder(self):
        t1 = Time(3600)
        t2 = Time(3600)
        t3 = Time(3601)
        t3 = Time(3602)
        self.assertTrue(t1==t2)
        self.assertTrue(t1==t2)
        self.assertFalse(t1!=t2)
        self.assertTrue(t1!=t3)
        self.assertTrue(t1<t3)
        self.assertTrue(t1<=t3)
        self.assertFalse(t1>t3)
        self.assertFalse(t1>=t3)

    def test_add(self):
        arg1 = 3600
        arg2 = 3601
        arg3 = 123
        t1 = Time(arg1)
        t2 = Time(arg2)
        t3 = Time(arg3)
        self.assertEqual((t1+t2).t,Time(arg1+arg2).t)
        self.assertTrue(t1+t2,Time(arg1+arg2))
        self.assertTrue(t2+t2,Time(arg1+arg2))
        self.assertTrue(t1+t2+t3,Time(arg1+arg2+arg3))