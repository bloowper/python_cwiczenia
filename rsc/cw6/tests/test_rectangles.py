from unittest import TestCase
from rsc.cw6.rectangles import Rectangle
from rsc.cw6.points import Point

class TestRectangle(TestCase):

    def setUp(self):
        self.r1 = Rectangle(0,0,1,1)
        self.r2 = Rectangle(0,0,1,1)
        self.r3 = Rectangle(0,0,2,2)
        self.r4 = Rectangle(-1,-1,1,1)
        self.r5 = Rectangle(0,0,4,4)

    def test_eq(self):
        self.assertTrue(self.r1 == self.r1)
        self.assertTrue(self.r1 == self.r2)
        self.assertFalse(self.r1 == self.r3)


    def test_center(self):
        self.assertEqual(self.r5.center(),Point(2,2))
        self.assertTrue(self.r4.center(),Point(0,0))


    def test_area(self):
        self.assertEqual(self.r4.area(),4)
        self.assertEqual(self.r5.area(),16)

    def test_move(self):
        r = Rectangle(0,0,1,1)
        self.assertEqual(r.move(1,1),Rectangle(1,1,2,2))
        r2 = Rectangle(-1,-1,1,1)
        self.assertEqual(r2.move(1,1),Rectangle(0,0,2,2))