from unittest import TestCase
from rsc.cw6.points import Point
import math

class TestPoint(TestCase):
    def test__str(self):
        point = Point(2, 2)
        self.assertEqual(str(point),"(2, 2)")
        self.assertNotEqual(str(point),"(2 , 2)")

    def test__repr(self):
        point = Point(2, 2)
        self.assertEqual(repr(point),"Point(2, 2)")

    def test_eq_ne(self):
        point1 = Point(2, 2)
        point2 = Point(2, 2)
        point3 = Point(3, 2)
        point4 = Point(2,3)
        #==
        self.assertTrue(point1==point1)
        self.assertTrue(point1==point2)
        self.assertFalse(point1==point3)
        self.assertFalse(point1==point4)
        #!=
        self.assertFalse(point1!=point2)
        self.assertFalse(point1!=point2)
        self.assertTrue(point1!=point3)
        self.assertTrue(point3!=point4)
        self.assertTrue(point4!=point3)

    def test_add(self):
        point1 = Point(1,1)
        point2 = Point(2,2)
        point3 = Point(-1,-1)
        self.assertEqual(point1+point2,Point(1+2,1+2))
        self.assertEqual(point1+point1,Point(1+1,1+1))
        self.assertEqual(point1+point3,Point(0,0))

    def test_sub(self):
        point1 = Point(1, 1)
        point2 = Point(2, 2)
        point3 = Point(-1, -1)
        self.assertEqual(point1-point2,Point(-1,-1))
        self.assertEqual(point2-point1,Point(1,1))
        self.assertEqual(point1-point3,Point(2,2))

    def test_mul(self):
        point1 = Point(1, 1)
        point2 = Point(2, 2)
        point3 = Point(-1, -1)
        point4 = Point(4,0)
        point5 = Point(0,4)
        self.assertEqual(point1*point2,4)
        self.assertEqual(point4*point5,0)

    def test_cross(self):
        point1 = Point(1, 1)
        point2 = Point(2, 2)
        point3 = Point(-1, -1)
        point4 = Point(4,0)
        point5 = Point(0,4)
        self.assertEqual(point1.cross(point1),0)
        self.assertEqual(point4.cross(point5),16)

    def test_lenght(self):
        point1 = Point(1, 1)
        point2 = Point(2, 2)
        point3 = Point(-1, -1)
        self.assertEqual(point1.length(),math.sqrt(1**2+1**2))
        self.assertEqual(point2.length(),math.sqrt(2**2 + 2**2))
        self.assertEqual(point3.length(),math.sqrt((-1)**2 + (-1)**2))

    def test_truediv(self):
        point = Point(4,8)
        self.assertEqual(point/2,Point(2,4))
