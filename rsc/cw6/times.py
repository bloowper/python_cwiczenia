import copy

#implementowałem bez zaglądania do implementacji Pana Profesora
#jedynie patrzyłem jakie moetody powinny sie pojawić

class Time:

    def __init__(self,t):
        """
        class initialization
        :param t: time
        """
        self.t = int(t)

    def __str__(self):
        """
        return string representation of Time class instance
        :rtype: str
        """
        time = copy.copy(self.t)
        h=0
        m=0
        s=0
        if time>=3600:
            h = time//3600
            time -= h*3600
        if time>=60:
            m = time//60
            time -= m*60
        if time>0:
            s = time
        return "H : {:<5}| M : {:<5}| S : {:<5}".format(h,m,s)
        #znalazlem taka fajna notacje :D ciekawa w sumie! i bardzie intuicyjna dla mnie

    def __repr__(self) -> str:
        #string bardziej formalny, dla programistow nie uzytkownikow( w dymysle)
        return f"Time({self.t})"

    def __add__(self, other):
        """
        :rtype: Time
        :type other: Time
        """
        return Time(self.t+other.t)

    def __sub__(self, other):
        """
        :rtype Time
        :type other: Time
        """
        return Time(self.t-other.t)

    def __cmp__(self, other):
        """
        :type other: Time
        """
        if not isinstance(other,Time):
            raise TypeError("other must be Time instance object")
        if self is other:
            return 0
        v = (self.t-other.t)
        if v>0:
            return 1
        if v<0:
            return -1
        else:
            return 0

    def __eq__(self, other):
        #==
        return self.t==other.t

    def __ne__(self, other):
        #!=
        return self.t != other.t

    def __gt__(self, other):
        #>
        return self.t>other.t

    def  __ge__(self, other):
        #>=
        return self.t>=other.t

    def __lt__(self, other):
        #<
        return self.t<other.t

    def __le__(self, other):
        #<=
        return self.t<=other.t

    #Python chyba >= <= sam umie typowac


if __name__ == '__main__':
        a = Time(3661)
        b = Time(3600)
        print(a)
        print(a.__repr__())
        print(a.__cmp__(b))
        print(b.__cmp__(a))
        print(b.__cmp__(b))
        print(b<=a)
        print(a>=b)



