import numpy as np
import math

class Point:
    """Klasa reprezentująca punkty na płaszczyźnie."""

    def __init__(self, x=0, y=0):  # konstuktor
        self.x = x
        self.y = y

    def __str__(self):          # zwraca string "(x, y)"
        return f"({self.x}, {self.y})"

    def __repr__(self):         # zwraca string "Point(x, y)"
        return f"{type(self).__name__}({self.x}, {self.y})"

    def __eq__(self, other):   # obsługa point1 == point2
        """
        :type other: Point
        """
        if not isinstance(other,Point):
            return False
        if self is other:
            return True
        return (self.x == other.x) and (self.y == other.y)

    def __ne__(self, other):        # obsługa point1 != point2
        return not self == other

    # Punkty jako wektory 2D.
    def __add__(self, other):  # v1 + v2
        return Point(self.x+other.x,self.y+other.y)

    def __sub__(self, other):   # v1 - v2
        """
        :rtype: Point
        """
        return Point(self.x - other.x, self.y - other.y)

    def __mul__(self, other):   # v1 * v2, iloczyn skalarny (liczba)
        return self.x * other.x + self.y * other.y

    def cross(self, other):         # v1 x v2, iloczyn wektorowy 2D (liczba)
        return self.x * other.y - self.y * other.x

    def dot(self,other):
        return self.x * other.x + self.y * other.y

    def length(self):          # długość wektora
        return math.sqrt(self.x**2 + self.y**2)

    def __truediv__(self, other):
        if isinstance(other,(int,float)):
            return Point(self.x/other,self.y/other)
        else:
            raise ValueError("dzielenie tylko przez skalar")

    def pointInBetween(self,other):
        """
        :type other: Point
        """
        return Point((self.x+other.x)/2,(self.y+other.y)/2)

    def lenghtBetweenPoints(self,other):
        """
        :type other: Point
        """
        return math.sqrt((self.x - other.x)**2 + (self.y - other.y)**2)

    def __hash__(self):
        return hash((self.x, self.y))   # bazujemy na tuple, immutable points

if __name__ == '__main__':
    point = Point(2, 2)
    print(repr(point))
    print(point.length())