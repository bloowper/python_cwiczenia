from unittest import TestCase
from rsc.cw5.poprawiony.fracs import *


class Test(TestCase):

    def test__add_frac(self):
        self.assertListEqual(
                            add_frac([1,2],[1,3]),
                            [5,6])
        self.assertListEqual(
                            add_frac([1,2],[-1,3]),
                            [1,6])
        self.assertListEqual(
                            add_frac([1,1],[1,1]),
                            [2,1])

    def test_sub_frac(self):
        self.assertListEqual(
            sub_frac([1, 2], [1, 3]),
            [1, 6])
        self.assertListEqual(
            sub_frac([1, 2], [-1, 3]),
            [5, 6])
        self.assertListEqual(
            sub_frac([1, 1], [1, 1]),
            [0, 1])

    def test_mul_frac(self):
        self.assertListEqual(
            mul_frac([1, 2], [1, 3]),
            [1, 6])
        self.assertListEqual(
            mul_frac([1, 2], [-1, 3]),
            [-1, 6])
        self.assertListEqual(
            mul_frac([1, 1], [1, 1]),
            [1, 1])

