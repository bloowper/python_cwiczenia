from unittest import TestCase
from rsc.cw5.poprawiony.polys import *


class Test(TestCase):

    def test_add_poly(self):
        w1 = [1,-3,2]
        w2 = [-1,3,-2,3]
        w3 = [0,0,0,3]
        self.assertListEqual(add_poly(w1,w2),w3)

    def test_sub_poly(self):
        w1 = [1,-3,2]
        w2 = [1,-3,2,-3]
        w3 = [0,0,0,3]
        poly = sub_poly(w1, w2)
        self.assertListEqual(poly,w3)

    def test_eval_poly(self):
        w1 = [1,-3,2]
        self.assertEqual(eval_poly(w1,0),1)
        self.assertEqual(eval_poly(w1,1),0)

    def test_eq_poly(self):
        self.assertEqual(eq_poly([0,0,3],[0,0,4]),-1)
        self.assertEqual(eq_poly([0,0,4],[0,0,3]),1)
        self.assertEqual(eq_poly([0,0,4],[0,0,4]),0)
        self.assertEqual(eq_poly([0,0,3,1],[0,0,4]),1)

    def test_diff_poly(self):
        self.assertListEqual(diff_poly([3,2,3]),[2,6])

