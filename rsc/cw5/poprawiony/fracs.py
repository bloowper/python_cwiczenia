import math
import numpy

def add_frac(frac1, frac2):
    l1,m1 = frac1
    l2,m2 = frac2
    lnew = l1*m2 + l2*m1
    mnew = m1 * m2
    gcd = numpy.gcd(lnew, mnew)
    lnew = lnew/gcd
    mnew = mnew/gcd
    return [lnew,mnew]


def sub_frac(frac1, frac2):        # frac1 - frac2
    return add_frac(frac1,[-frac2[0],frac2[1]])

def mul_frac(frac1, frac2):        # frac1 * frac2
    l1,m1 = frac1
    l2,m2 = frac2
    lnew = l1 * l2
    mnew = m1 * m2
    gcd = numpy.gcd(lnew, mnew)
    lnew = lnew/gcd
    mnew = mnew/gcd
    return [lnew,mnew]

def div_frac(frac1, frac2):
    return mul_frac(frac1,[frac2[1],frac2[0]])

def is_positive(frac):
    l1,m1 = frac
    return l1*m1>0

def is_zero(frac):                 # bool, typu [0, x]
    return frac[0]==0

def cmp_frac(frac1, frac2):        # -1 | 0 | +1
    l1,m1 = frac1
    l2,m2 = frac2
    return l1==l2 and m1==m2

def frac2float(frac):              # konwersja do float
    l1,m1 = frac1
    return l1/m1

if __name__ == '__main__':
    print(div_frac([1,2],[1,2]))