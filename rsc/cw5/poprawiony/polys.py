import copy



def add_poly(p1, p2):
    poly1 = p1[::-1]
    poly2 = p2[::-1]
    shorter = poly1 if len(poly1)<len(poly2) else poly2
    newpoly = copy.deepcopy(poly1) if len(poly1)>len(poly2) else copy.deepcopy(poly2)
    for i in range(1,len(shorter)+1):
        newpoly[-i] = newpoly[-i] + shorter[-i]
    return newpoly[::-1]

def sub_poly(poly1, poly2):

    poly2 = [-x for x in poly2]
    return add_poly(poly1,poly2)

def mul_poly(poly1, poly2):
    pass        # poly1(x) * poly2(x)

def is_zero(poly):
    for i in poly:
        if i != 0:
            return False
    return True

def eq_poly(poly1, poly2):
    if len(poly1)>len(poly2):
        return 1
    elif len(poly1)<len(poly2):
        return -1
    #ta notacja wielomianu zapisywana od czynnika wolnego jest niewygodna :/
    for p1,p2 in reversed(list(zip(poly1,poly2))):
        if p1>p2:
            return 1
        elif p1<p2:
            return -1
    return 0

def eval_poly(poly, x0):
    p = poly[::-1]
    value = p[0]
    for i in range(1,len(p)):
        value=value*x0 + p[i]
    return value


def combine_poly(poly1, poly2):
    pass    # poly1(poly2(x)), trudne!

def pow_poly(poly, n):
    pass             # poly(x) ** n

def diff_poly(poly):
    newp = [0]*(len(poly)-1)
    for i in range(1,len(poly)):
        newp[i-1] = poly[i]*i
    return newp

