

if __name__ == '__main__':
    #1
    # import rsc.cw5.rekurencja as rek
    # print(rek.factorial(10))
    # print(rek.fibonacci(10))

    # #2
    # from rsc.cw5.rekurencja import *
    # print(factorial(10))
    # print(fibonacci(10))

    from rsc.cw5.rekurencja import fibonacci as fib
    from rsc.cw5.rekurencja import factorial
    print(fib(10))
    print(factorial(10))
