
import math
import copy
# Docstringi sa glownie dla mnie zeby
# lepiej pisalo mi sie testy, gdyz gdy pycharm zna typ zwracany
# to moze podpowiadac metody ktore mozna wywolac na obiekcie
# co jest dla mnie niezmiernie pomocne, i przez to sie nie gubie w kodzie tak bardzo

class frac:
    def __init__(self, numerator, denominator):
        if denominator==0:
            raise ValueError("denominator cant be 0")
        self.numerator = numerator
        self.denominator = denominator

    def __add__(self, other):
        """
        :type other: frac
        :rtype : frac
        """
        lcm = (self.denominator * other.denominator) / math.gcd(self.denominator, other.denominator)
        return frac(self.numerator * (lcm / self.denominator) + other.numerator * (lcm / other.denominator), lcm)

    def __sub__(self, other):
        """
        :type other: frac
        :rtype : frac
        """
        lcm = (self.denominator * other.denominator)/math.gcd(self.denominator,other.denominator)
        return frac(self.numerator * (lcm/self.denominator) - other.numerator*(lcm/other.denominator),lcm)


    def __mul__(self, other):
        """
        :type other: frac,int,float
        :rtype : frac
        """
        if(isinstance(other,frac)):
            f = frac(self.numerator * other.numerator, self.denominator * other.denominator)
            f.normalize()
            return f
        elif(isinstance(other,(int,float))):
            return frac(self.numerator*other,copy.copy(self.denominator))
                    #mam pytanie. jezeli jest operator * to on zwraca nowy obiekt
                    # a self.denominator poprostu przepisalo by mi referencje(shalow coppy)
                    #zgadza sie?S

    def __truediv__(self, other):
        """
        :type other: frac
        :rtype : frac
        """
        f = frac(self.numerator * other.denominator, self.denominator * other.numerator)
        f.normalize()

        return f

    def normalize(self):
        gcd = math.gcd(self.numerator,self.denominator)
        self.numerator = (self.numerator/gcd)
        self.denominator = (self.denominator/gcd)

    def is_positive(self):
        return self.numerator * self.denominator>0

    def is_zero(self):
        return self.numerator==0

    #nie jest to dobra implementacja
    #powinienem sprowadzic do wspolnego mianownika
    #i porownywac liczniki, natomiast mialem sporo obowiazkow a malo czasu
    #dlatego zostalem na takiej implementacji
    def cmp(self,other):
        return self.tofloat()-other.tofloat()

    def tofloat(self):
        if self.numerator==0: return 0
        return self.numerator/self.denominator

    def __rep__(self):
        return f"{self.numerator}\\{self.denominator}"

    def __str__(self):
        return f"{self.numerator}\\{self.denominator}"



if __name__ == '__main__':
    f1 = frac(1,2)
    f2 = frac(3,4)
    f1f2 = f1 * f2
    print(f1f2)


