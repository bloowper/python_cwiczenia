from unittest import TestCase
from rsc.cw5.frac import *

class Testfracs(TestCase):

    def test_init(self):
        f1_1 = frac(1, 1)
        self.assertEqual(f1_1.numerator,1)
        self.assertEqual(f1_1.denominator,1)
        self.assertEqual(f1_1.numerator/f1_1.denominator,1/1)
        f2_1 = frac(2, 1)
        self.assertEqual(f2_1.numerator,2)
        self.assertEqual(f2_1.denominator,1)
        self.assertEqual(f2_1.numerator/f2_1.denominator,2/1)
        self.assertRaises(ValueError, frac, 2, 0)

    def test_toflat(self):
        fracs0 = frac(10, 2)
        fracs1 = frac(20, 2)
        fracs2 = frac(30, 2)
        self.assertEqual(fracs0.tofloat(),10/2)
        self.assertEqual(fracs1.tofloat(),20/2)
        self.assertEqual(fracs2.tofloat(),30/2)

    def test_normalize(self):
        frac0 = frac(128, 256)
        frac0.normalize()
        self.assertEqual(frac0.numerator,1)
        frac1 = frac(16, 4)
        frac1.normalize()
        self.assertEqual(frac1.numerator,4)
        self.assertEqual(frac1.denominator,1)

    def test_add(self):
        frac1 = frac(1,2)
        frac2 = frac(1,4)
        frac12 = frac1+frac2
        self.assertEqual(frac12.numerator,3)
        self.assertEqual(frac12.denominator,4)

        frac_ = frac(1, 3) + frac(6, 7)
        self.assertEqual(frac_.numerator,25)
        self.assertEqual(frac_.denominator,21)

    def test_sub(self):
        frac1 = frac(1,2)
        frac2 = frac(1,4)
        frac1_2 = frac1 - frac2
        self.assertEqual(frac1_2.numerator,1)
        self.assertEqual(frac1_2.denominator,4)

        frac3 = frac(5,6)
        frac4 = frac(1,4)
        frac3_4 = frac3-frac4
        self.assertEqual(frac3_4.numerator,7)
        self.assertEqual(frac3_4.denominator,12)

    def test_truediv(self):
        f1 = frac(1, 2)
        f2 = frac(3, 4)
        f1f = f1/f2
        self.assertEqual(f1f.numerator,2)
        self.assertEqual(f1f.denominator,3)

        f3 = frac(9,12)
        f4 = frac(8,12)
        f3f4 = f3 / f4
        self.assertEqual(f3f4.numerator,9)
        self.assertEqual(f3f4.denominator,8)

    def test_mul(self):
        f1 = frac(1, 2)
        f2 = frac(3, 4)
        f1_f2 = f1*f2
        self.assertEqual(f1_f2.numerator,3)
        self.assertEqual(f1_f2.denominator,8)

        f19 = f1 * 9
        self.assertEqual(f19.numerator,9)
        self.assertEqual(f19.denominator,2)


