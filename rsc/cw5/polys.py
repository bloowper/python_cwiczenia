import math
import copy

class Pol:
    """

    """

    def __init__(self, lis=None):
        if lis is None:
            self.pol = list()
        else:
            self.pol = lis

    def evaluate(self,x):
        eval = 0
        for pow,coef in enumerate(self.pol):
            if pow==0:
                eval+=coef
                continue
            eval+=(coef*math.pow(x,pow+1))
        return eval

    def __add__(self, other):
        """
        :rtype: Pol
        :type other: Pol
        """
        if len(other.pol)>len(self.pol):
            #bazowy other
            long = copy.copy(other.pol)
            short = self.pol
        else:
            #bazowy self
            long = copy.copy(self.pol)
            short = other.pol
        for i in range(len(long)):
            if i < len(short):
                long[i] = long[i] + short[i]
        return Pol(long)

    def __str__(self):
        str = ""
        for pow,coef in enumerate(self.pol):
            if pow == 0:
                str += f"{+coef}*X^{pow}"
                continue
            str+=f" +{+coef}*X^{pow}"
        return str

    def __repr__(self):
        print("Pol{ "+self.pol+" }")

if __name__ == '__main__':
    pol1 = Pol([3, 2, 1])
    pol2 = Pol([3,3,3,1])
    pol12 = pol1 + pol2
    print(pol1)
    print(pol2)
    print(pol12)

