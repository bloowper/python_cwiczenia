def factorial(n):
    start = 1
    for i in range(1, n + 1):
        start *= i
    return start


def fibonacci(n):
    n1 = 1
    n2 = 1
    for i in range(n - 2):
        temp = n2
        n2 = n2 + n1
        n1 = temp
    return n2
