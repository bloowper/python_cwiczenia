import random
import numpy as np
import itertools
import copy
import matplotlib.pyplot as plt


def randomList(n):
    """
    zwraca liste elementow od 0 do n-1 w kolejnosci losowej
    :param n: koniec przedzialu
    :type n: int

    :return: lista liczb od 0 do n-1 w kolejnosci losowej
    :rtype: list

    """
    t = list(range(0,n))
    random.shuffle(t)
    return t

#TODO uogulnic do wiekszych mozliwych odstepstw od ustawienia wlasciwego aktualnie jest to 3. zewnetrzne moga zmienic o dwie pozycje srodkowy o jedna w lewo lub prawo
def almost_sorted(n):
    if n%3 == 0:
        # dziele zbior na podzbiory w tym przypadku troj elementowe i je bede shuflowac
        # nie wiem czy pomysl jest dobry ale zastanawialem sie chwile i na nic wiecej nie wpadlem
        x:np.ndarray = np.arange(n).reshape(n//3,3)
        for i in range(x.shape[0]):
            np.random.shuffle(x[i])
        return x.flatten().tolist()
    else:
        x: np.ndarray = np.arange((n//3+1)*3).reshape(n // 3+1, 3)
        lastRow : np.ndarray = copy.copy(x[-1])
        newLastRow = []
        for elem in lastRow:
            if elem<n:
                newLastRow.append(elem)
        lastRow = newLastRow
        np.random.shuffle(lastRow)

        x = np.delete(x,-1,0)
        for i in range(x.shape[0]):
            np.random.shuffle(x[i])
        x = np.append(x,lastRow)
        return x

def almost_sorted_reverse(n):
    sorted = almost_sorted(n)
    flip = np.flip(sorted)
    return flip

def gauss(n):
    list = []
    for i in range(n+1):
        list.append(np.random.normal())
    return list

def zPowtorzeniami(n, k):
    l = []
    for _ in range(n+1):
        l.append(random.randint(0,k))
    return l
if __name__ == '__main__':

    z_powtorzeniami = zPowtorzeniami(1000, 20)
    plt.plot(z_powtorzeniami,".")
    plt.savefig("z_powtorzeniami n=1000 k =20")
    plt.show()




