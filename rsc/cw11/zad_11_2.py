

import matplotlib; matplotlib.use("TkAgg")
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from rsc.cw11.createList import *


fig, ax = plt.subplots()


x = gauss(500)
line, = ax.plot(x)


def swap(L, left, right):
    """Zamiana miejscami dwóch elementów."""
    # L[left], L[right] = L[right], L[left]
    item = L[left]
    L[left] = L[right]
    L[right] = item


def animate(i):
    def bubblesort(L, left, right):
        limit = right
        while True:
            k = left - 1  # lewy wskaźnik przestawianej pary
            for i in range(left, limit):
                if L[i] > L[i + 1]:
                    swap(L, i, i + 1)
                    k = i
                    return L
            if k > left:
                limit = k
            else:
                break

    bubblesort(x,0,len(x))

    line.set_ydata(x)  # update the data
    return line,


# Init only required for blitting to give a clean slate.
def init():
    line.set_ydata(np.ma.array(x, mask=True))
    return line,

ani = animation.FuncAnimation(fig, animate, np.arange(1, 20000), init_func=init,
                              interval=1, blit=True)
plt.show()