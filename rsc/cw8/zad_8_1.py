#Implementacja rozwiazywania ukladu rownan liniowych
#Przy uzyciu metody Gaussa Jordana
#Zakladam ze kolumna wyrazow wolnych bedzie znana
#a priori i nie bedzie potrzeby rozwiazywania
#ukladu ponownie dla innej kolumny wyrazow wolnych
#Dlatego implementacja Gaussa Jordana do tego problemu bedzie zasadna
#Zakladam rowniez ze nie znamy kształtu macierzy A zatem nie mozemy
#Zaimplementowac forward i backward substitution w formie uwzgledniajacej
#ksztalt macierzy
#No i fajnie gdyby uklad nie byl oznaczony

import numpy
from numpy.core._multiarray_umath import ndarray

def solve(A,b):
    """Solve system of linear equations
    :return return vector(matrix) with solve of linear system
    :rtype: numpy.ndarray
    :type A: numpy.ndarray
    :type b: numpy.ndarray
    """
    ab = numpy.hstack([A, b])

    # forward substitution
    for k in range(0,A.shape[0]-1):
        for w in range(k+1,A.shape[0]):
            ab[w] = ab[w] - ab[k]*(1/ab[k,k])*ab[w,k]
    print(ab,end='\n\n')

    # backward substitution
    for k in reversed(range(0,A.shape[0])):
        ab[k] =ab[k] * 1/ab[k,k]
        for w in range(k-1,-1,-1):
            ab[w] = ab[w] - ab[k]*ab[w,k]
    return ab[:,ab.shape[0]:]


if __name__ == '__main__':
    # Ax=b
    A: numpy.ndarray = numpy.array([
        [1, 2, 3, 4],
        [2, 1, 2, 3],
        [3, 2, 1, 2],
        [4, 3, 2, 1]
    ])
    B = numpy.array([
        [5,9],
        [1,8],
        [1,7],
        [-1,6],
    ])
    print(f"Ax=b\nA:\n{A}\n\nB:\n{B}")
    X = solve(A, B)
    print(f"wektory rozwiazan\n{X}\n")
    multiply = numpy.dot(A, X)
    print(f"Sprawdzmy: A*X\n{multiply}")