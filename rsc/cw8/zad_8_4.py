import itertools
import math

def heron(a,b,c):
    l = sorted([a,b,c])
    if not l[0]+l[1]>l[2]:
        raise ValueError("niespelniony warunek trojkata")
    return math.sqrt((a+b+c)*(a+b-c)*(a-b+c)*(-a+b+c))/4


if __name__ == '__main__':
    print(heron(3,3,3))
