# ZADANIE 8.6
# Za pomocą techniki programowania dynamicznego napisać
# program obliczający wartości funkcji P(i, j).
# Porównać z wersją rekurencyjną programu.
# Wskazówka: Wykorzystać tablicę dwuwymiarową (np. słownik)
# do przechowywania wartości funkcji. Wartości w tablicy
# wypełniać kolejno wierszami.
#
# P(0, 0) = 0.5,
# P(i, 0) = 0.0 dla i > 0,
# P(0, j) = 1.0 dla j > 0,
# P(i, j) = 0.5 * (P(i-1, j) + P(i, j-1)) dla i > 0, j > 0.
# i=0 j=0   0.5
# i>0 j=0   0
#i=0 j>0    1
#

import time


class IllegalArgumentError(ValueError):
    pass

#odziedzicze sobie po klasie dict zeby przeciazyc getitem dla wartosci
#brzegowych ktore sa zdefiniowane dla wiekszej ilosci przypadkow
#niz tylko pojedynczy punkt
class dictZad8(dict):
    def __getitem__(self,k):
        """
        :type k: tuple
        """
        if not isinstance(k,tuple):
            raise IllegalArgumentError("argument have to be tuple")
        if len(k)!=2:
            raise IllegalArgumentError("ilegal size of tuple")
        i = abs(k[0])
        j = abs(k[1])
        if (i,j) == (0,0):
            return 0.5
        if i>0 and j==0:
            return 0
        if i==0 and j>0:
            return 1
        return super().__getitem__(k)

    def __contains__(self, k) -> bool:
        """
        :type k: tuple
        """
        if not isinstance(k,tuple):
            raise IllegalArgumentError("argument have to be tuple")
        if len(k)!=2:
            raise IllegalArgumentError("ilegal size of tuple")
        i = abs(k[0])
        j = abs(k[1])
        if (i,j) == (0,0):
            return True
        if i>0 and j==0:
            return True
        if i==0 and j>0:
            return True
        return super().__contains__(k)


def Pdynamically(i,j):
    # gdy brzegowe pojawia sie przy samym wywolaniu
    if (i, j) == (0, 0):
        return 0.5
    if i > 0 and j == 0:
        return 0
    if i == 0 and j > 0:
        return 1
    dic = dictZad8()

    def P(i,j):
        nonlocal dic
        subscriptor1 = (i-1,j)
        subscriptor2 = (i,j-1)
        if not subscriptor1 in dic:
            dic[subscriptor1] = P(*subscriptor1)
        if not subscriptor2 in dic:
            dic[subscriptor2] = P(*subscriptor2)
        dic[i,j] = (0.5 * (dic[subscriptor1] + dic[subscriptor2]))
        return dic[i,j]
    P(i,j)
    return dic[(i,j)]

def Pstatic(i,j):
    if (i, j) == (0, 0):
        return 0.5
    if i > 0 and j == 0:
        return 0
    if i == 0 and j > 0:
        return 1
    # P(i, j) = 0.5 * (P(i-1, j) + P(i, j-1)) dla i > 0, j > 0.
    return 0.5 * (Pstatic(i-1,j) + Pstatic(i,j-1))





if __name__ == '__main__':
    toCompute = (10,15)
    #juz dla 20 20 obliczenia trwaly ponad 30 minut
    td1 = time.time()
    d = Pdynamically(*toCompute)
    td2 = time.time()

    ts1 = time.time()
    s = Pstatic(*toCompute)
    ts2 = time.time()
    print(f" wartosc obliczona dynamicznie: {d}\n"
          f"wartosc obliczona statycznie: {s}")
    print(f"dynamicznie czas : {td2-td1}\n",
          f"statycznie czas : {ts2-ts1}")