from unittest import TestCase
from rsc.cw8.zad_8_6 import dictZad8


class TestdictZad8(TestCase):
    def test_getitem(self):
        dic = dictZad8()
        # i == 0 j ==0
        self.assertEqual(dic[(0, 0)], 0.5)

        # i>0 j==0
        self.assertEqual(dic[(1, 0)], 0)
        self.assertEqual(dic[(32131, 0)], 0)
        self.assertEqual(dic[(100, 0)], 0)
        self.assertEqual(dic[(1030, 0)], 0)
        self.assertEqual(dic[(3, 0)], 0)
        self.assertEqual(dic[(4, 0)], 0)

        # i==0 j>0
        self.assertEqual(dic[(0, 1)], 1)
        self.assertEqual(dic[(0, 2)], 1)
        self.assertEqual(dic[(0, 3)], 1)
        self.assertEqual(dic[(0, 4)], 1)
        self.assertEqual(dic[(0, 5)], 1)
        self.assertEqual(dic[(0, 6)], 1)
        self.assertEqual(dic[(0, 100)], 1)
        self.assertEqual(dic[(0, 3213123)], 1)

    def test__contains(self):
        dic = dictZad8()
        # i=0 j=0   0.5
        # i>0 j=0   0
        # i=0 j>0    1
        self.assertTrue((0,0) in dic)
        for i in range(1,10000):
            self.assertTrue((i,0) in dic)
        for j in range(1,10000):
            self.assertTrue((0,j) in dic)


