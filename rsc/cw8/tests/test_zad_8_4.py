from unittest import TestCase
from rsc.cw8.zad_8_4 import heron
import math

def inbound(receivedValue,expectedValue,accurency):
    return expectedValue-accurency\
           <=receivedValue<=\
           expectedValue+accurency


class Test(TestCase):
    def test_heron_error(self):
        self.assertRaises(ValueError,lambda :heron(100,2,2))

    def test_heron(self):
        self.assertTrue(inbound(
            receivedValue=heron(3,3,3),
            expectedValue=3.90,
            accurency=0.1))
        self.assertTrue(inbound(
            receivedValue = heron(5,4,math.sqrt(41)),
            expectedValue = 5*4*1/2,
            accurency=0.1
        ))