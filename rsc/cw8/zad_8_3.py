import random
from matplotlib import pyplot as plt
import math

#wymagany pakiet matplotlib
#pip install matplotlib


#z tej klasy trzeba zrobic po prostu metode :)
class PI_computing:
    def __init__(self,liczbaLosowychPunktow):
        #pi = 4 * w_kole / wszystkie
        liczbaTrafien = 0
        for _ in range(liczbaLosowychPunktow):
            (x,y) = random.random(),random.random()
            if math.sqrt(x**2 + y**2)<=1:
                liczbaTrafien+=1;
        self.PI = 4 * liczbaTrafien/liczbaLosowychPunktow



if __name__ == '__main__':
    """
    OX liczba eksperymentow wykonanych
    OY wartosc PI wynikajaca z metody montecarlo od liczby losowych punktow
    """

    liczby_pomiarow = []


    #czestrze probkowanie w zakresie
    #5-500
    #1000-1500
    #10 000 - 10 500
    #100 000 - 100 500

    liczby_pomiarow.extend(list(range(5,500,5))) #probkowanie miedzy 5-500
    liczby_pomiarow.extend(list(range(1_000,1_501,5)))  #probkowanie miedzy 1.000 1.301
    liczby_pomiarow.extend(list(range(10_000, 10_501, 5))) #probkowanie miedzy 10.000 10301

    wartosc_PI = []


    #ZAKRESY OD wartosci pi zeby wydziec jak wartosc obliczona zbiega do wartosci prawdziwej wraz ze wzrostem liczby powtorzen eksperymentu
    #na wartsoc pi
    plt.plot(liczby_pomiarow, [math.pi] * len(liczby_pomiarow))

    plt.xlabel("Ilosc pomiarow")
    plt.ylabel("wartosc PI")
    plt.title("Obliczanie wartosci PI przy uzyciu metody Monte Carlo")

    for ilosc_powtorzen in liczby_pomiarow:
        wartosc_PI.append(PI_computing(ilosc_powtorzen).PI)



    plt.plot(liczby_pomiarow,wartosc_PI,linewidth=0.3)
    plt.plot(liczby_pomiarow,[math.pi+ 0.025]*len(liczby_pomiarow),"r--",linewidth=0.7)
    plt.plot(liczby_pomiarow,[math.pi - 0.025]*len(liczby_pomiarow),r"--",linewidth=0.7)
    plt.savefig("ilosc_pomiarow->wartosc_pi.png",dpi=900,)
    plt.show()
