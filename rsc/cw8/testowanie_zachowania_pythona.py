import numpy
import itertools

if __name__ == '__main__':
    m1 = numpy.array([[1, 1], [2, 3]])
    print(m1)
    m2 = numpy.hstack([m1, [[1],[1]]])
    n = numpy.array([[1, 2, 3, 10], [1, 2, 3, 10], [1, 2, 3, 10]])
    print(n[:,3:])
    a = (1,2,3)
    combinations = itertools.combinations(a, 2)
    print(type(combinations))
    for elem in combinations:
        e = [x for x in a if x not in elem]
        print(f"{elem} | {e[0]}")
    tu = (1,2,3)
    a,b,c = tu
    print(a)
    c = {1:"ababa",2:"bababab"}
    c_ = c[3]
    print(c_)
