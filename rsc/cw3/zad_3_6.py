# ZADANIE 3.6
# Napisać program rysujący prostokąt zbudowany z małych kratek. Należy zbudować pełny string, a potem go wypisać. Przykładowy prostokąt składający się 2x4 pól ma postać:
#
# +---+---+---+---+
# |   |   |   |   |
# +---+---+---+---+
# |   |   |   |   |
# +---+---+---+---+


def prostokat(x,y):
    def array2dToStr(array):
        temp = ""
        for line in array:
            for char in line:
                temp += str(char)
            temp += "\n"
        return temp
    (xsize,ysize) = 5,5
    tablica = [[" "]*(x*xsize+1)]*(y*ysize+1)
    for wiersz in range(len(tablica)):
        for kolumna in range(len(tablica[wiersz])):
            if wiersz%ysize==0 and kolumna%xsize==0:
                tablica[wiersz][kolumna]="+"
            elif kolumna%xsize==0:
                tablica[wiersz][kolumna] = "|"
                #znudzilo mi sie szukanie dlaczego to sie nie wykonuje
            elif wiersz%ysize==0:
                tablica[wiersz][kolumna]="-"
    return array2dToStr(tablica)



if __name__ == '__main__':
    print(prostokat(4,2))
