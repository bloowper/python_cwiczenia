# ZADANIE 3.8
# Dla dwóch sekwencji znaleźć: (a) listę elementów występujących
# jednocześnie w obu sekwencjach (bez powtórzeń), (b) listę wszystkich elementów z obu sekwencji (bez powtórzeń).



if __name__ == '__main__':
    l = [1, 2, 3, 23, 345, 23, 654, 23]
    l1 = [1, 2, 4, 234, 5345, 12312, 5435, 2]
    print(set(l) & set(l1))
    print(set(l) | set(l1))