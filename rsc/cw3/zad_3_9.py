
def zlicz_sekwencje(sekwencja):
    to_return = 0
    for elem in sekwencja:
        to_return+=elem
    return to_return

if __name__ == '__main__':
    l = [[], [4], (1, 2), [3, 4], (5, 6, 7)]
    sumy = []
    for sekwencja in l:
        sumy.append(zlicz_sekwencje(sekwencja))
    print(sumy)