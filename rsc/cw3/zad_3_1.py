
if __name__ == '__main__':
    x = 2;
    y = 3;
    if (x > y):
        result = x;
    else:
        result = y;

    print(f"x {x} y {y} result {result}")
    """
    kod jest poprawny poniewaz
    1)mimo ze semicolon nie jest wymagany w pythonie to moze on odzielac instrukcie i nie jest to traktowane
    jako blad
    2) () nie sa uznawane za poprawne w instrukcjach warunkowych ale znowu
    ich uzycie nie jst traktowane jako blad skladniowy
    3) tego troche nie rozumiem.... referencja result powinna istniec tylko w zakresie if i else...
    a istnieje poza
    """

    # for i in "qwerty": if ord(i) < 100: print(i)
    """
    nie jest poprawne 
    """

    for i in "axby": print(ord(i) if ord(i) < 100 else i)
    """
    jest poprawne poniewaz dla kazdego elementu w stringu
    wykonujemy print wartosci UTF tego znaku GDY spelniony jest warunek
    w przeciwnym razie wypisujemy ten znak
    OPIS POGLADOWY
    """