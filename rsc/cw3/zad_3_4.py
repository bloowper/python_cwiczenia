# ADANIE 3.4
# Napisać program pobierający w pętli od użytkownika liczbę rzeczywistą x (typ float)
# i wypisujący parę x i trzecią potęgę x.
# Zatrzymanie programu następuje po wpisaniu z klawiatury stop.
# Jeżeli użytkownik wpisze napis zamiast liczby, to program ma wypisać komunikat o błędzie i kontynuować pracę.

if __name__ == '__main__':
    str = None;
    while str!="stop":
        str=input()
        try:
            f = float(str)
            print(f"{f} -> {pow(f,3)}")
        except ValueError:
            print("to nie liczba typu float")
