import math

# ZADANIE 3.5
# Napisać program rysujący "miarkę" o zadanej długości. Należy prawidłowo obsłużyć liczby składające się z
# kilku cyfr (ostatnia cyfra liczby ma znajdować się pod znakiem kreski pionowej). Należy zbudować pełny string, a potem go wypisać.
#
# |....|....|....|....|....|....|....|....|....|....|....|....|
# 0    1    2    3    4    5    6    7    8    9   10   11   12

def miarka(dlugosc):
    #z racji tego ze stringi sa immutable powinienem wykorzystac jakiegos string buildera
    #zeby zminimalizowac uzycie pamieci, ale pomine ta optymalizacje
    line1 = "|..."*dlugosc+"|"
    line2=""
    for i in range(dlugosc+1):
        if i == 0:
            line2+="0"
            continue
        line2+=f"{i:4}"
    return line1+"\n"+line2




if __name__ == '__main__':
    print(miarka(20))