import math
import copy
import numpy

#implementuje od nowa nie zagladajac w poprzednia implementacje
#dla treningu :)



class Frac:
    """Klasa reprezentująca ułamki."""

    def __init__(self, x=0, y=1):
        # Sprawdzamy, czy y=0.
        if isinstance(x,float) or isinstance(y,float):
            if y == 0:
                raise ValueError("Divide by 0 is not allowed")
            if isinstance(x,float) or isinstance(y,float):
                asIntRatioX = float.as_integer_ratio(float(x))
                asIntRatioY = float.as_integer_ratio(float(y))
                self.x = asIntRatioX[0] * asIntRatioY[1]
                self.y = asIntRatioX[1] * asIntRatioY[0]
            self.x = x
            self.y = y
            gcd = math.gcd(self.x,self.y)
            self.x = self.x / gcd
            self.y = self.y / gcd
        else:
            if y == 0:
                raise ValueError("Divide by 0 is not allowed")
            gcd = math.gcd(x,y)
            self.x = x/gcd
            self.y = y/gcd

    def __str__(self):
        # zwraca "x/y" lub "x" dla y=1
        if self.y == 1:
            return str(self.x)
        else:
            return f"{self.x}/{self.y}"

    def __repr__(self):
        # zwraca "Frac(x, y)"
        return f"{Frac.__name__}({self.__dict__})"

    # Python 2
    #def __cmp__(self, other): pass  # cmp(frac1, frac2)

    # Python 2.7 i Python 3
    def __eq__(self, other):
        if isinstance(other,Frac):
            if id(self) == id(other):
                return True
            if self.x == other.x and self.y == other.y:
                return True
            else:
                return False
        if isinstance(other,int):
            if self.y != 1:
                return False
            else:
                return self.x == other

    def __ne__(self, other):
        return not self == other

    def __lt__(self, other):
        return self.x/self.y < other.x/other.y

    def __le__(self, other):
        return self.x/self.y <= other.x/other.y

    #def __gt__(self, other): pass

    #def __ge__(self, other): pass

    def __add__(self, other):   # frac1+frac2, frac+int
        #dodac implementacje frac+float
        if isinstance(other,int):
            if other==0:
                return copy.copy(self)
            return Frac(copy.copy(self.x)+other*self.y,copy.copy(self.y))
        elif isinstance(other,Frac):
            numerator = int(self.x * other.y + other.x * self.y)
            denumerator = int(self.y * other.y)
            gcd = math.gcd(numerator,denumerator)
            return Frac(numerator/gcd,denumerator/gcd)
        else:
            raise AttributeError(f"nie zdefiniowano dla {type(other)}")

    __radd__ = __add__              # int+frac

    def __sub__(self, other):  # frac1-frac2, frac-int
        if isinstance(other,int):
            return self+(-other)
        elif isinstance(other,Frac):
            return self+Frac(-other.x,other.y)
        else:
            raise AttributeError(f"nie zdefiniowano dla {type(other)}")

    def __rsub__(self, other):      # int-frac
        # tutaj self jest frac, a other jest int!
        return Frac(self.y * other - self.x, self.y)

    def __mul__(self, other):  # frac1*frac2, frac*int
        if isinstance(other,int):
            return Frac(self.x*other,self.y)
        elif isinstance(other,Frac):
            return Frac(self.x * other.x,
                        self.y * other.y)
        else:
            raise AttributeError(f"nie zdefiniowano dla {type(other)}")

    __rmul__ = __mul__              # int*frac

    def __div__(self, other):  # frac1/frac2, frac/int, Python 2
        if isinstance(other,int):
            return Frac(self.x,self.y * other)
        elif isinstance(other,Frac):
            return Frac(self.x * other.y,self.y * other.x)
        else:
            raise AttributeError(f"nie zdefiniowano dla {type(other)}")


    def __rdiv__(self, other):  # int/frac, Python 2
            #div self/other
            #rdiv other/self z tego wyniknal niejednoznacznosci..
            if isinstance(other,int):
                return Frac(other*self.y,self.x)
            elif isinstance(other,Frac):
                return Frac(other.x * self.y,other)
            else:
                raise AttributeError(f"nie zdefiniowano dla {type(other)}")

    def __truediv__(self, other):   # frac1/frac2, frac/int, Python 3
        if isinstance(other,int):
            return Frac(self.x,self.y * other)
        elif isinstance(other,Frac):
            return Frac(self.x * other.y,self.y * other.x)
        else:
            raise AttributeError(f"nie zdefiniowano dla {type(other)}")

    def __rtruediv__(self, other):  # int/frac, Python 3
        # div self/other
        # rdiv other/self z tego wyniknal niejednoznacznosci..
        if isinstance(other, int):
            return Frac(other * self.y, self.x)
        elif isinstance(other, Frac):
            return Frac(other.x * self.y, other)
        else:
            raise AttributeError(f"nie zdefiniowano dla {type(other)}")
##TUTAJ SKONCZYLEM
##################

    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        return Frac(numpy.abs(self.x),self.y)

    def __neg__(self):          # -frac = (-1)*frac
        return Frac(-self.x,self.y)

    def __invert__(self):       # odwrotnosc: ~frac
        if self.x<0:
            return Frac(-self.y,numpy.abs(self.x))
        else:
            return Frac(self.y,self.x)

    def __float__(self):        # float(frac)
        return self.x / self.y

    def __hash__(self):
        return hash(float(self))   # immutable fracs
        # assert set([2]) == set([2.0])


if __name__ == '__main__':
    print(Frac(1,2)-Frac(3,4))
    print(Frac(1,2)-1)
    print(1-Frac(1,2))
    print(Frac(1,2)*Frac(1,2))
    print(2*Frac(1,2))
    print(Frac(1,2)*2)
    print(Frac(1,2) / Frac(1,2))
    print(2/Frac(1,2))
    print(numpy.abs(-2))