from unittest import TestCase
from rsc.cw7.fracs import Frac

#Myli mi sie kolejnosc (Oczekiwana, uzysana) wartosc w tescie

class TestFrac(TestCase):

    def test__init__(self):
        frac1 = Frac(2, 1)
        self.assertEqual(frac1.x, 2)
        self.assertEqual(frac1.y, 1)
        frac2 = Frac(4,6)
        self.assertEqual(frac2.x,2)
        self.assertEqual(frac2.y,3)
        frac3 = Frac(1/2,2)
        self.assertEqual(frac3.x,1)
        self.assertEqual(frac3.y,4)
        frac4 = Frac(2,1/2)
        self.assertEqual(frac4.x,4)
        self.assertEqual(frac4.y,1)
        self.assertRaises(ValueError,lambda :Frac(1,0))
        self.assertRaises(ValueError,lambda :Frac(1/2,0))
        self.assertRaises(ValueError,lambda :Frac(0/2,0/2))

    def test__eq__(self):
        self.assertTrue(Frac(1/2) == Frac(1,2/1))
        self.assertFalse(Frac(1/2) == Frac(1,3))
        ## 1/4 1/8 / 1/2
        self.assertTrue(Frac(1,4) == Frac(1,4))
        self.assertTrue(Frac(10,2) == Frac(5))
        self.assertFalse(Frac(10,2) == Frac(9,2))

    def test__ne__(self):
        self.assertFalse(Frac(1,2)!=Frac(2,4))
        self.assertTrue(Frac(1,2)!=Frac(2,2))

    def test__lt__le(self):
        self.assertTrue(Frac(1,2)>Frac(1,3))
        self.assertFalse(Frac(1,2)>Frac(1,2))
        self.assertTrue(Frac(3,4)<Frac(1/2,2/10))
        self.assertTrue(Frac(1,2)<=Frac(1,2))
        self.assertFalse(Frac(1,2)<Frac(1,2))

    def test__add__(self):
        self.assertEqual(Frac(5,2),Frac(3,2)+1)
        self.assertNotEqual(Frac(5,2),Frac(3,3)+1)
        self.assertEqual(Frac(3,10),Frac(3,10)+0)

    def test_sbu_rsub(self):
        self.assertEqual(Frac(1,2)-1,Frac(-1,2))
        self.assertEqual(1-Frac(1,2),Frac(1/2))
        self.assertRaises(AttributeError,lambda :Frac(1,2)+"string")

    def test__mul__(self):
        self.assertEqual(Frac(1,2)*Frac(1,2),Frac(1,4))
        self.assertEqual(Frac(1,2)*2,Frac(1))
        self.assertEqual(2*Frac(1,2),Frac(1))
        self.assertRaises(AttributeError,lambda :Frac(10,13)*"string")

    def test__divALL(self):
        self.assertEqual(2/Frac(1,2),4)
        self.assertEqual(Frac(1,2)/2,Frac(1,4))

