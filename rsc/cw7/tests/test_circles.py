from unittest import TestCase
from rsc.cw7.circles import Circle
from math import *

class TestCircle(TestCase):



    # def setUp(self) -> None:
    #     super().setUp()

    def test_area(self):
        #skorzystam z chain comparison
        #jest dla mnie dalej dziwna notacja
        #w jezyku programowania
        #nie bede uzywac assert equels z powodu
        #bledow zaokroglen
        c1 = Circle(0, 0, 2)
        self.assertTrue(pi*2**2-0.001<=c1.area()<=pi*2**2+0.001)

    def test_move(self):
        c1 = Circle(0,0,1)
        c1.move(1,1)
        self.assertEqual(c1,Circle(1,1,1))

    def test_cover(self):
        c1 = Circle(1,0,1)
        c2 = Circle(-1,0,1)
        c3 = Circle.cover(c1,c2) #takie uzycie wydaje mi sie bardziej zasadne
        #c1.cover(c2)
        self.assertEqual(c3,Circle(0,0,2))

        #implementacje cover nie dziala prawidlowo

        c1 = Circle(-1,-1,1)
        c2 = Circle(1,1,1)
        c3 = Circle(0,0,1+sqrt(2))

    def test_cover_2(self):
        c1 = Circle(0,-1,1)
        c2 = Circle(0,1,1)
        c3 = Circle.cover(c1,c2)
        self.assertEqual(Circle(0,0,2),c3)
