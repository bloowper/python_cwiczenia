import copy


#implementuje od nowa bez podgladania nigdzie :) dla treningu

#TODO zmienic sel.wielomian z typu list na tym numpy.array
class Poly:
    """ Klasa reprezentująca wielomiany.
        Zakladam ze operacje typu add ect zwracaja nowy wielomian
        immutable Poly
    """


    #zmieniam implementacje na tablice
    #jest dla mnei wygodniejsza
    def __init__(self, tablica):
        self.wielomian = tablica
        self.xRep = "x"
        #d + cx + bx^2 + ax^3
        #[d,c,b,a]

    # TODO zaimplementowac
    def __str__(self):
        #TODO zmienic implementacje na jakis string builder zeby nie marnowac pamieci
        #uzywam nagminie str() majac nadzieje ze pomzoe mi to pozniej z zagniezdzonymi wielomianami
        strToReturn = ""
        for (potega,wspolczynnik) in enumerate(self.wielomian):
            if potega == 0:
                strToReturn += f"{str(wspolczynnik)}"
            else:
                strToReturn += f" + {str(wspolczynnik)}*{str(self.xRep)}^{potega}"
        return strToReturn


    # TODO zaimplementowac
    def __add__(self, other):   # poly1+poly2, poly+int POLY+FLOAT
        """
        :type other: Poly or int or float
        """
        if isinstance(other,(int,float)):
            w = copy.deepcopy(self.wielomian)
            w[0] +=other
            return Poly(w)
        if isinstance(other,Poly):
            w1: list = copy.deepcopy(self.wielomian)
            w2: list = copy.deepcopy(other.wielomian)
            if(len(w1)<len(w2)):
                w1,w2 = w2,w1
            #chcemy iterowac przez indeksy mniejszej listy
            for i in range(0,len(w2)):
                w1[i]+=w2[i]
            return Poly(w1)

    __radd__ = __add__              # int+poly

    # TODO zaimplementowac
    def __sub__(self, other):   # poly1-poly2, poly-int
        pass

    # TODO zaimplementowac
    def __rsub__(self, other):  # int-poly
        pass

    # TODO zaimplementowac
    def __mul__(self, other):   # poly1*poly2, poly*int
        pass

    __rmul__ = __mul__              # int*poly

    # TODO zaimplementowac
    def __pos__(self):              # +poly1 = (+1)*poly1
        return self

    # TODO zaimplementowac
    def __neg__(self):         # -poly1 = (-1)*poly1
        pass

    # TODO zaimplementowac
    def is_zero(self):          # bool, True dla [0], [0, 0],...
        pass

    # TODO zaimplementowac
    def __eq__(self, other):    # obsługa poly1 == poly2
        pass

    # TODO zaimplementowac
    def __ne__(self, other):        # obsługa poly1 != poly2
        return not self == other

    # TODO zaimplementowac
    def eval(self, x):          # schemat Hornera
        pass

    # TODO zaimplementowac
    def combine(self, other):       # złożenie poly1(poly2(x))
        pass

    # TODO zaimplementowac
    def __pow__(self, n):       # poly(x)**n lub pow(poly(x),n)
        pass

    # TODO zaimplementowac
    def diff(self):             # różniczkowanie
        pass

    # TODO zaimplementowac
    def integrate(self):        # całkowanie
        pass

    # TODO zaimplementowac
    def __len__(self):          # len(poly), rozmiar self.a
        pass

    # TODO zaimplementowac
    def __getitem__(self, i):             # poly[i], współczynnik przy x^i
        pass

    # TODO zaimplementowac
    def __setitem__(self, i, value):      # poly[i] = value
        pass

    # TODO zaimplementowac
    def __call__(self, x): pass     # poly(x)
    # dla isinstance(x, (int,long,float)) odpowiada eval(),
    # dla isinstance(x, Poly) odpowiada combine()

if __name__ == '__main__':
    p1 = Poly([2, 3, 4])
    p2 = Poly([3,2,6])
    p3 = p1+p2
    print(p1)
    print(p2)
    print(p3)
    print(p3+5)