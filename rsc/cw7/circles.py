from rsc.cw6.points import Point
import math

class Circle:
    """Klasa reprezentująca okręgi na płaszczyźnie."""

    def __init__(self, x, y, radius):
        if radius < 0:
            raise ValueError("promień ujemny")
        self.pt = Point(x, y)
        self.radius = radius

    def __repr__(self):
        return f"{Circle.__name__}({self.pt.x}, {self.pt.y}, {self.radius})"

    def __eq__(self, other):
        return self.pt == other.pt and self.radius == other.radius

    def __ne__(self, other):
        return not self == other

    def area(self):            # pole powierzchni
        return math.pi * self.radius**2

    def move(self, x, y):     # przesuniecie o (x, y)
        self.pt.x+=x
        self.pt.y+=y

    #TODO poprawic cover. ma chyba problem gdy kat miedzy wektorem a ox wynosi pi/2
    #wyglada na to ze problem wystepuje gdy wektor laczacy srodki okregow na kat z
    #osia OX rowny PI/2
    def cover(self, other):   # najmniejszy okrąg pokrywający oba
        """
        :type other: Circle
        """

        # nie umiem teo zrobic :(
        #TODO NAPRAWIC PRZYPADEK BRZEGOWY
        #
        # Circle(0.0, 1.0, 0.0) != Circle(0, 0, 2)
        # FAILED (failures=1)
        #sytuacja gdy circle1 circle2 leza na tym samym x
        if self.pt.x == other.pt.x:
            #P1 ma miec y wieksze od P2
            if(self.pt.y >= other.pt.y):
                circleP1 = Circle(self.pt.x,self.pt.y,self.radius)
                circleP2 = Circle(other.pt.x,other.pt.y,other.radius)
            else:
                circleP2 = Circle(self.pt.x, self.pt.y, self.radius)
                circleP1 = Circle(other.pt.x, other.pt.y, other.radius)

            xWsp = self.pt.x
            przesuniecieP1 = Point(xWsp,circleP1.radius)
            przesuniecieP2 = Point(xWsp,circleP2.radius)
            srodek = przesuniecieP1.pointInBetween(przesuniecieP2)
            newradius = srodek.lenghtBetweenPoints(przesuniecieP1)
            return Circle(srodek.x,srodek.y,newradius)
        ########################################## koniec sytuacji "brzegowej"
        OX = Point(1,0)
        przesuniecieSelf = Point(self.radius * OX.dot(self.pt)/(OX.length() * self.pt.length()),
                                 self.radius * OX.cross(self.pt)/(self.pt.length()))
        przesuniecieOther = Point(other.radius * other.pt.dot(OX)/(other.pt.length()),
                                  other.radius *other.pt.cross(OX)/other.pt.length() )
        selfPoitPrim = self.pt+przesuniecieSelf
        otherPointPrim = other.pt+przesuniecieOther
        srodekPoint = Point.pointInBetween(selfPoitPrim,otherPointPrim)
        newRadius = Point.lenghtBetweenPoints(srodekPoint,selfPoitPrim)
        return Circle(srodekPoint.x,srodekPoint.y,newRadius)

if __name__ == '__main__':
    c1 = Circle(1, 0, 1)
    c2 = Circle(-1, 0, 1)
    cover = c1.cover(c2)
    print(cover)