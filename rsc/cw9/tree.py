#typow w doc stringu uzywam dla szybszego pisania tekstu

class Node:
    """Klasa reprezentująca węzeł drzewa binarnego."""

    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)

    def setLeftChild(self,left):
        """
        :type left: Node or Null
        """
        self.left = left

    def setRightChild(self,right):
        """
        :type right: Node or Null
        """
        self.right=right



#Wersje rokurencyjne nie sa na pewno tak wydajne
#jak iteracyjne
#natomiast pierwsze to mi wpadlo to glowo
#to tez to zaimplemetowalem :-)

def count_leafs(top):
    """
    :type top: Node
    """
    if top==None:
        raise ValueError("Drzewo musi byc conajmniej korzeniem")
    numOfLeafs = 1
    def deepCount(node: Node):
        nonlocal numOfLeafs
        if node.left!=None:
            numOfLeafs+=1
            deepCount(node.left)
        if node.right!=None:
            numOfLeafs+=1
            deepCount(node.right)
    deepCount(top)
    return numOfLeafs

#TODO implementacja iteracyjna, na stosie
def count_leafs_iteracyjnie(top):
    """
    :type top: Node
    """
    pass

def calc_total(top):
    """
    :type top: Node
    """
    if top == None:
        raise ValueError("Drzewo musi byc conajmniej korzeniem")
    sum = top.data
    def deep_add(node: Node):
        nonlocal sum
        if node.left != None:
            sum = sum+node.left.data
            deep_add(node.left)
        if node.right != None:
            sum = sum+node.right.data
            deep_add(node.right)

    deep_add(top)
    return sum

#TODO napisac iteracyjnie
def calc_total_iteracyjnie(top):
    pass




