
class Node:
    """Klasa reprezentująca węzeł listy jednokierunkowej."""

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)   # bardzo ogólnie




class SingleList:
    """Klasa reprezentująca całą listę jednokierunkową."""
    tail: Node
    head: Node

    def __init__(self):
        self.length = 0   # nie trzeba obliczać za każdym razem
        self.head  = None
        self.tail  = None

    def is_empty(self):
        #return self.length == 0
        return self.head is None

    def count(self):   # tworzymy interfejs do odczytu
        return self.length

    def insert_head(self, node):
        if self.head:   # dajemy na koniec listy
            node.next = self.head
            self.head = node
        else:   # pusta lista
            self.head = self.tail = node
        self.length += 1

    def insert_tail(self, node):   # klasy O(N)
        if self.head:   # dajemy na koniec listy
            self.tail.next = node
            self.tail = node
        else:   # pusta lista
            self.head = self.tail = node
        self.length += 1

    def remove_head(self):          # klasy O(1)
        if self.is_empty():
            raise ValueError("pusta lista")
        node = self.head
        if self.head == self.tail:   # self.length == 1
            self.head = self.tail = None
        else:
            self.head = self.head.next
        node.next = None   # czyszczenie łącza
        self.length -= 1
        return node   # zwracamy usuwany node


    def remove_tail(self) -> Node:   # klasy O(N)
        if self.is_empty():
            raise ValueError("pusta lista")
        if self.head == self.tail:
            temp = self.head
            self.head = None
            self.tail = None
            self.length = 0
            return temp
        #w tym momenicie wiadomo ze jest co najmniej dlugosc 2
        node = self.head.next
        nodePrev = self.head
        while node != self.tail:
            node = node.next
            nodePrev = nodePrev.next
        nodePrev.next=None
        self.tail = nodePrev
        return node

    def merge(self, other):
        """
        :type other: SingleList
        """
        while True:
            try:
                otherhead = other.remove_head()
                self.insert_tail(otherhead)
            except ValueError:
                break

    #sprobuje uzyc protokolu iteracyjnego
    #nie mialem okazji jeszcze go uzywac
    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        """
        :rtype: Node
        """
        if self.current == None:
            raise StopIteration
        temp = self.current
        self.current = self.current.next
        return temp

    def search(self, data):
        for elem in self:
            if elem.data == data:
                return elem

    def find_min(self):
        if self.head == None:
            return None
        min = self.head.data
        for elem in self:
            if elem.data<min:
                min = elem
        return min

    def find_max(self):
        if self.head == None:
            return None
        max = self.head.data
        for elem in self:
            if elem.data>max:
                max = elem.data
        return max

    def reverse(self):
        pass


if __name__ == '__main__':
    l = SingleList()
    l.insert_tail(Node(2))
    l.insert_tail(Node(5))
    l.insert_tail(Node(2))
    l.insert_tail(Node("okon"))
    l.insert_tail(Node(3))
    l.insert_tail(Node(5))
    for e in l:
        print(e)

    print(l.search("okon"))
    print(l.search(111))

    l2 = SingleList()
    l2.insert_tail(Node(2))
    l2.insert_tail(Node(22))
    l2.insert_tail(Node(222))
    l2.insert_tail(Node(2222))
    print(l2.find_min())
    print(l2.find_max())
