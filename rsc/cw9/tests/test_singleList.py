from unittest import TestCase
from rsc.cw9.singleList import SingleList, Node


class TestSingleList(TestCase):
    def test_remove_tail(self):
        l = SingleList()
        l.insert_tail(Node("jeden"))
        l.insert_tail(Node("dwa"))
        l.insert_tail(Node("trzy"))
        self.assertEqual(l.remove_tail().data, "trzy")
        self.assertEqual(l.remove_tail().data, "dwa")
        self.assertEqual(l.remove_tail().data, "jeden")
        self.assertRaises(ValueError, lambda: l.remove_tail())


    def test_merge(self):
        l1 = SingleList()
        l1.insert_tail(Node("1"))
        l1.insert_tail(Node("2"))
        l1.insert_tail(Node("3"))
        l2 = SingleList()
        l2.insert_tail(Node("4"))
        l2.insert_tail(Node("5"))
        l2.insert_tail(Node("6"))
        l1.merge(l2)
        self.assertEqual(l1.length,6)
        self.assertEqual(l2.length,0)

        self.assertEqual(l1.remove_tail().data,"6")
        self.assertEqual(l1.remove_tail().data,"5")
        self.assertEqual(l1.remove_tail().data,"4")
        self.assertEqual(l1.remove_tail().data,"3")
        self.assertEqual(l1.remove_tail().data,"2")
        self.assertEqual(l1.remove_tail().data,"1")
        self.assertRaises(ValueError, lambda: l1.remove_tail())



