from unittest import TestCase
import rsc.cw9.tree as tree

class Test(TestCase):
    def test_count_leafs(self):
        t2 =\
        tree.Node(data=1,
                  left=
                    tree.Node(
                        data=2,
                        left=
                            tree.Node(3),
                        right=
                            tree.Node(4)
                    ),
                  right=
                      tree.Node(
                          data=5,
                          left=
                                tree.Node(6),
                          right=
                                tree.Node(7)
                      ))
        self.assertEqual(tree.calc_total(t2),1+2+3+4+5+6+7)
        self.assertEqual(tree.count_leafs(t2), 7)