


def count_words(words):
    return len([word for word in words.split() if (len(word)>1 or word in "i")])


if __name__ == '__main__':
    test = "testowy string testowy string  tstowy , string test hej hej" \
           "\n test test, -test test test . hej test"
    print(count_words(test))

    test2 = "jeden dwa trzy , cztery , piec . szesc , siedem . osiem, dziewiec / dziesiet i jedenascie"
    print(count_words(test2))

