import re

# ZADANIE 2.16
# W tekście znajdującym się w zmiennej line zamienić ciąg znaków "GvR" na "Guido van Rossum".


# zadanie wykonane po terminie
if __name__ == '__main__':
    line = "By default, PyCharm highlights all detected code problems.****** GvR <------\n" \
            " Hover the mouse over the widget in top-right corner of the editor and select another level from the Highlight list:"
    print(line)
    print("-"*30)
    print(re.sub("GvR","Guido van Rossum",line))
