# ZADANIE 2.15
# Na liście L znajdują się liczby całkowite dodatnie. Stworzyć napis będący ciągiem cyfr kolejnych liczb z listy L.

def listToIntSequence(lis):
    return "".join([str(x) for x in lis])

if __name__ == '__main__':
    myList = [123,432,123,54,23,534,123,765]
    print(listToIntSequence(myList))
    myList2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    print(listToIntSequence(myList2))