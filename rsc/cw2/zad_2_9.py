

def copy_wh_comments(f,t):
    with open(f,"r") as fr:
        with open(t,"w") as to:
            for line in fr:
                if not line.startswith("#"):
                    to.write(line)



if __name__ == '__main__':
    """testowanie funkcjonalnosci funkcji\n
    docelowo chce to przeniesc na jakis framework\n
    uwazam ze testy powinny byc oddzielone od implementacji"""
    fFrom = "from.txt"
    fTo = "to.txt"
    copy_wh_comments(fFrom,fTo)
    print(f"zawartosc {fFrom}\n{20*'-'}")
    print("".join(open(fFrom,"r").readlines()))
    print(f"{20*'-'}")
    print(f"zawartos {fTo} po skopiowaniu\n"
          f"{20*'-'}")
    print("".join(open(fTo,"r").readlines()),f"\n{20*'-'}")

    # czyszczenie pliku do nastepnego wywolania testu
    print(f"CZYSZCZENIE PLIKU {fTo}")
    open(fTo,"w").close()