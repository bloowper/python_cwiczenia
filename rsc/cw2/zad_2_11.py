

def str_underscore(word,podzial = "_"):
    return podzial.join([char for char in word])

if __name__ == '__main__':
    print(str_underscore("jakiesSlowo"))