
import re
# ZADANIE 2.14
# Znaleźć: (a) najdłuższy wyraz, (b) długość najdłuższego wyrazu w napisie line.

def najdluzszyWyraz(line):
    return max([(len(x), x) for x in re.split(r"[^\w']+", line)],key=lambda x:x[0])[1]

def dlugoscNajdluzszegoWyrazu(line):
    return max([(len(x), x) for x in re.split(r"[^\w']+", line)],key=lambda x:x[0])[0]


if __name__ == '__main__':
    line =     line = "jeden, dwa, trzy cztery piec szesc\n" \
           "okon losos Sum najdluzszy" # 10 najdluzszy
    line_ = [(len(x),x) for x in re.split(r"[^\w']+", line)]
    print(f"najdluzszy wyraz to : {najdluzszyWyraz(line)}")
    print(f"dlugosc najdluzszego to : {dlugoscNajdluzszegoWyrazu(line)}")