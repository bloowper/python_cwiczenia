import re


# ZADANIE 2.13
# Znaleźć łączną długość wyrazów w napisie line. Wskazówka: można skorzystać z funkcji sum().

def lenOfWords(line):
    return sum([len(x) for x in re.split(r"[^\w']+", line)])


if __name__ == '__main__':
    line = "jeden, dwa, trzy cztery piec szesc\n" \
           "okon losos Sum"
    print(f"poprawna dlugosc wynosi {len('jedendwatrzyczterypiecszescokonlosossum')}")
    print(f"dlugosc zwracana przez funkcje {lenOfWords(line)}")