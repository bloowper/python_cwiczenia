import re
# Posortować wyrazy z napisu line raz alfabetycznie, a raz pod względem długości. Wskazówka: funkcja wbudowana sorted().


if __name__ == '__main__':
    line = """Hit Below The Belt
    Meaning: A boxing term. Also often used to refer to inappropriate words, or comments that are too personal.
    Like Father Like Son
    Meaning: Resembling one's parents in terms of appearance or behavior.
    Roll With the Punches
    Meaning: To tolerate or endure through the unexpected mishappenings you may encounter from time to time.
    You Can't Judge a Book By Its Cover
    Meaning: Don't judge someone or something only by the outward appearance.
    Jig Is Up
    Meaning: For a ruse or trick to be discovered; to be caught."""
    #tekst z random phrese generator
    split = re.split(r"[^\w']+", line)
    split.pop()
    l =  sorted(split)
    print(l)
    l = sorted(split,key=len)
    print(l)



